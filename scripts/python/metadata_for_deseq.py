"""
This script generates a metadata csv file for DESeq2
it essentially requires a sampleName (unique ID), fileName 
(basename of the file within an all-countaining directory...), 
and any number of co-variates (we use only condition), 
but add cell_line for subsetting in R...

# manual assembly in R (essentially what this script now takes over...)
sampleTable <- data.frame(sampleName = sampleFiles,
                          fileName = sampleFiles,
                          condition = sampleCondition)
"""

import os, glob
import pandas as pd
import re 

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
REPLICATES = os.path.join(USER_DIR, "replicates.csv")
HTSEQ_DIR = os.path.join(USER_DIR, "HTSEQ")

def read_replicates(filename:str) -> pd.DataFrame:
    """
    Reads the replicates.csv file and gets the groups and associated replciates in a dataframe
    """
    replicates = pd.read_csv(filename, sep = ";", names = ["Sample", "Replicates"])
    replicates["Replicates"] = [i.split(" ") for i in replicates["Replicates"]]
    return replicates


if __name__ == "__main__":
    
    reps = read_replicates(REPLICATES)
    samples = reps["Sample"]

    metadata = {"sampleName" : [], "fileName" : [],  "condition" : [],  "cell_line" : []}

    for sample in samples:
        subset = reps.query(f"Sample == '{sample}'")
        subset = list(subset["Replicates"])
        
        idx = 1
        cell_line, condition = sample.split("_")
        for i in subset:
            for j in i:
                count_file = f"{j}_quant.tsv"

                metadata["sampleName"].append(f"{sample}_{idx}")
                metadata["fileName"].append(count_file)
                metadata["condition"].append(condition)
                metadata["cell_line"].append(cell_line)

                idx += 1

    metadata = pd.DataFrame(metadata)
    metadata.to_csv(os.path.join(USER_DIR, "scripts/r/deseq_index.csv"), index = False) 
    print(metadata)
    print("Metadata file successfully generated!")