"""
This script will generate a metadata sample info file for sleuth to read
"""

import pandas as pd 
import os, glob 

USER_DIR = "/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR = os.path.join(USER_DIR , "reads")
REPLICATES = os.path.join(USER_DIR, "replicates.csv")

GROUPED_DIR = os.path.join(USER_DIR , "KALLISTO_grouped")
UNGROUPED_DIR = os.path.join(USER_DIR , "KALLISTO")


def read_replicates(filename:str) -> pd.DataFrame:
    """
    Reads the replicates.csv file and gets the groups and associated replciates in a dataframe
    """
    replicates = pd.read_csv(filename, sep = ";", names = ["Sample", "Replicates"])
    replicates["Replicates"] = [i.split(" ") for i in replicates["Replicates"]]
    return replicates


class SampleReader:
    """
    This class will handle assembling the metadata
    """
    def __init__(self):
        self._df = None
        self._replicates = None
        self._index_dict = { "sample": [],  # the replicate HD1 ...
                        "cell_line" : [],  # the cell line A549 ...
                        "condition" : [], # CTR KO ...
                        "path" : [] # directory where abundace file is stored for sample ...
                    }
        self._save_to = None
        self._ungrouped_loc = None
    
    def get(self):
        """
        Returns the metadata dataframe
        """
        return self._df

    def setup(self, save_to, sample_dir):
        """
        Setup directories for saving the index and for grouped samples
        """
        self._save_to = save_to
        self._ungrouped_loc = sample_dir

    def replicates(self, filename:str):
        """
        Link a replicates csv file
        """
        reps = read_replicates(filename)
        self._replicates = reps

    def index(self):
        """
        Assembles the metadata dataframe 
        """
        if self._replicates is not None and self._ungrouped_loc is not None and self._save_to is not None:
            self._index()
        else:
            raise Exception("Please, first specify a set of replicates and sample directory for your datset!")

    def save(self):
        """
        Saves the metadata dataframe
        """
        if self._df is not None:
            self._df.to_csv(os.path.join(self._save_to, "index.csv"), index=False)
        else:
            raise Exception("You have not yet indexed your samples! Run .index() before saving.")

    def _index(self):
        """
        Assembles the metadata dataframe
        """
        for exp_condition, replicates in zip(self._replicates["Sample"], self._replicates["Replicates"]):
            
            for replicate in replicates: 
                sample_loc = os.path.join(self._ungrouped_loc, replicate)
                cell_line, condition = exp_condition.split("_")

                self._index_dict["sample"].append(replicate)
                self._index_dict["cell_line"].append(cell_line)
                self._index_dict["condition"].append(condition)
                self._index_dict["path"].append(sample_loc)

            self._df = pd.DataFrame(self._index_dict)
    
    
if __name__ == "__main__": 

    reader = SampleReader()
    reader.replicates(REPLICATES)
    reader.setup(
        save_to = os.path.join(USER_DIR, "scripts/r"),
        sample_dir = UNGROUPED_DIR
    )
    reader.index()
    reader.save()