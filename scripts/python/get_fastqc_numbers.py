"""
This script extracts the FASTQC per-base-quality numbers from the FASTQC report-txt files and stores them in a single table...
"""

import pandas as pd
import os 
import glob
from zipfile import ZipFile
import shutil
import subprocess
import re 

# with ZipFile(path_to_zip_file, 'r') as zip_ref:
#     zip_ref.extractall(directory_to_extract_to)


READS_DIR = "/data/courses/rnaseq/lncRNAs/Project1/users/noah/reads"

os.chdir(READS_DIR)

total_reads_grep = "Total Sequences	(\d+)"
bad_reads_grep = "Sequences flagged as poor quality	([0-9]+)"

per_base_grep = "#(Base	Mean	Median	Lower Quartile	Upper Quartile	10th Percentile	90th Percentile\n[0-9.\t\n]+)>>END_MODULE"
# per_base_grep = "Total Sequences	(\d+)"

reads_stats = {
    "rep" : [],
    "total" : [],
    "bad" : [],
    "read" : [],
}

per_base_qualities = None

for i in os.listdir():
    
    if not os.path.isdir(i): continue
    
    # get fastqc zip files
    qc_dir = os.path.join(READS_DIR, i, "multiqc", "fastqc")
    zip_files = glob.glob(os.path.join(qc_dir, "*.zip"))

    for z in zip_files:
        zname = os.path.basename(z).replace(".zip", "")
        tmp_dir = os.path.join(qc_dir, "tmp")
        with ZipFile(z, 'r') as zip_ref:
            zip_ref.extractall(tmp_dir)

        data_file = f"{qc_dir}/tmp/{zname}/fastqc_data.txt"
        with open(data_file, "r") as f:
            content = f.read()
            total_reads = re.search(total_reads_grep, content).group(1)
            bad_reads = re.search(bad_reads_grep, content).group(1)
            reads_stats["read"].append(zname.split("_001_")[0])
            reads_stats["rep"].append(os.path.basename(i))
            reads_stats["total"].append(int(total_reads))
            reads_stats["bad"].append(int(bad_reads))


            if per_base_qualities is None: 
                new_entries = re.search(per_base_grep, content).group(1)
                with open(f"{data_file}.tmp", "w") as f1:
                    f1.write(new_entries)
                per_base_qualities = pd.read_table(f"{data_file}.tmp", sep = "\t")
                per_base_qualities["replicate"] = [zname.split("_001_")[0] for _ in per_base_qualities["Base"]]
                os.remove(f"{data_file}.tmp")
            else:
                new_entries = re.search(per_base_grep, content).group(1)
                with open(f"{data_file}.tmp", "w") as f1:
                    f1.write(new_entries)
                new_entries = pd.read_table(f"{data_file}.tmp", sep = "\t")
                new_entries["replicate"] = [zname.split("_001_")[0] for _ in new_entries["Base"]]
                per_base_qualities = pd.concat([per_base_qualities, new_entries])
    
    shutil.rmtree(tmp_dir)

per_base_qualities[["Base", "Mean", "replicate"]].to_csv(os.path.join(READS_DIR, "per_base_qualities.csv"), index = False)
reads_stats = pd.DataFrame(reads_stats)
reads_stats.to_csv(os.path.join(READS_DIR, "reads_total.csv"), index = False)
 
