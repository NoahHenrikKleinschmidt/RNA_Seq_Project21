"""
This script converts the chromosome names of the bed file of polyA sites from 1 2 3 to chr1 chr2 etc...
"""
import pandas as pd

input_file = "/data/courses/rnaseq/lncRNAs/Project1/users/noah/CAGER_ref/atlas.clusters.2.0.GRCm38.96.bed"
output_file = "/data/courses/rnaseq/lncRNAs/Project1/users/noah/CAGER_ref/polyA_processed.bed"

df = pd.read_table(input_file, sep = "\t", names = [chr(i+65) for i in range(11)])


print(df)

df["A"] = df["A"].apply(lambda x: f"chr{x}")
df["K"] = df["K"].apply(lambda x: x if x == x else "NA")


print(df)

df.to_csv(output_file, index = False, header = False, sep = "\t")

