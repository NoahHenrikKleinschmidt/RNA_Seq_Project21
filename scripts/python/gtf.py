"""
This module is designed to assist with working with GTF meta-assembly files for quality control purposes 

The basic architecture of a gtf file looks like this: 
sequence_name   source_method  feature_type    start   stop score   strand  reading_frame   attributes  
NC_000001.11	StringTie	exon	184878	185350	1000	-	.	gene_id "STRG.6"; transcript_id "STRG.6.1"; exon_number "1"; reference_id "rna-XR_001737556.1"; ref_gene_id "gene-WASH9P"; ref_gene_name "WASH9P"; cov "14.191861";

The main purpose of this module is to extract valuable information from this file architexture and place it into a pandas dataframe

"""

import pandas as pd 
import sys
import re 

# if sys.version_info[0] < 3: 
#     from StringIO import StringIO
# else:
#     from io import StringIO


# =================================================================
# Preprocessing
# =================================================================

# GTF header added by pandas later...
# GTF_HEADER = "seqname\tsource_method\tfeature\tstart\tend\tscore\tstrand\treading_frame\tattribute"

# def _read_file(filename):
#     # opens a given file and returns a string of all non-comment lines
#     with open(filename, 'r') as openfile:
#         content = openfile.read().split("\n")
#     # remove comments
#     content = [i for i in content if not i.startswith("#")]
#     # content.insert(0, GTF_HEADER)
#     content = "\n".join(content)
#     return content


# if only specific attributes should be extracted from the gtf attribute column 
# then specify a valid list with names of the given attribute of interest
ATTRIBUTES = {"gene_id" : "gene_id", 
              "transcript_id" : "transcript_id", 
              "exon_number" : "exon_number", 
              "reference_id" : "reference_id", 
              "ref_gene_id" : "ref_gene_id",
              "ref_gene_name" : "ref_gene_name",
            #   "GeneID" : "GeneID",
            #   "ID" : "ID",
              "gene_name" : "gene_name",
        }


def read_gtf(filename, add_header=True):
    """
    This function will read the given file, and generate a df 
    """
    if add_header:
        col_names = ["sequence_name", "source_method", "feature", "start", "stop", "score", "strand", "reading_frame", "attribute"]
        dtypes = {"sequence_name" : str, "source_method" : str, "feature" : str, "start" : int, "stop" : int, "score" : str, "strand" : str , "reading_frame" : str , "attribute" : str}
        df = pd.read_table(filename, sep = "\t", names = col_names, comment = "#", dtype = dtypes)
    else:
        df = pd.read_table(filename, sep = "\t", comment = "#")
    # df = df.to_dict(orient="list")
    df = _update_data(df)
    return df


def _get_attribute(string, attr_id):
    """
    Uses regex to find an attribute in the attribute column, 
    this function is used with pandas apply()
    """
    try: 
        group_id = 3 if "(" in attr_id else 2
        if re.search(attr_id, string) is not None: 
            pattern = f"{attr_id}(=| )\"?([A-Za-z0-9.-_]+)\"?;"
            value = re.search(pattern, string).group(group_id)
            return value
        else: 
            return None
    except:
        return None

def _update_data(df):
    """
    Applies the _get_attribute for all attributes
    """
    for attr_id, attr_pattern in ATTRIBUTES.items():
        df[attr_id] = df["attribute"].apply(_get_attribute, attr_id = attr_pattern)
    return df

# def _update_data(df):
#     """
#     This function will extract info from the attribute column 
#     to add to the overall dataframe and make the info searchable

#     Check out the extract_from_attribute fuction for more info
#     """
#     new_df = {}

#     for i in df["attribute"]:
        
#         extracted_attribute = _extract_from_attribute(i)
#         for i in extracted_attribute:
#             val = extracted_attribute[i]
#             if i not in new_df:
#                 new_df[i] = [val]
#             else: 
#                 new_df[i].append(val)

    
#     df.update(new_df)
#     df = pd.DataFrame(df)
#     return df

    
# def _prep_tmp_dict(row):
#     """
#     Splits the input row and generates a dictionary of found attributes
#     """
#     try: 
#         tmp = row.split(";")
#         tmp = [i.strip() for i in tmp]
#         tmp = [i.replace('"', "") for i in tmp]

#         try: 
#             tmp.remove("")
#         except: pass

#         # try to split the attribute field based on space, if that doesnt change anything, try with =
#         if " " in tmp[0]:
#             tmp = [i.split(" ") for i in tmp]
#         elif "=" in tmp[0]:
#             tmp = [i.split("=") for i in tmp]
#         else:
#             raise("The GTF File contains unknown attribute-identifier separators! Only known ones are space or =!")

#         tmp_dict = {i[0] : i[1] for i in tmp}

#     except: 
#         print("At this line there is an error: ", row)
#         exit(1)

#     return tmp_dict

# def _extract_from_attribute(row):
#     """ 
#     This function extracts information from attribute column
#     It will extract all entries specified in the ATTRIBUTES
#     """    
#     tmp_dict = _prep_tmp_dict(row)
    
#     results = {}
#     for attr in ATTRIBUTES: 
#             val = _val_to_none(attr, tmp_dict)
#             results.update(
#                 { attr : val }
#             )
#     return results

# def _val_to_none(attr, tmp_dict):
#     """
#     Checks if a given attribute is specified in tmp_dict
#     if not it returns None
#     """
#     try: 
#         return tmp_dict[attr]
#     except: 
#         return None

def drop_attributes(df):
    df.drop(["attribute"], axis = 1)
    return df


# =================================================================
# Informational Extraction
# =================================================================

def get_stats(df):
    """
    This function gets the number of distinct genes in the assembly
    It returns a dictionary containing: 
        1. number of entries within the assembly (i.e. all entries)
        2. number of distinct transcript_id entries (i.e. unique transcripts)
        3. number of distinct ref_gene_name  
        4. number of transcripts with a mapped ref_gene_name
        5. number of ref_gene_name transcripts with only one exon
        6. number of entries without ref_gene_name
    """
    
    transcripts_df = df.query("feature == 'transcript'")

    # 1. number of entries in assembly 
    total_transcripts = len(df["transcript_id"])

    # 2. number of distinct transcripts
    n_transcripts = len(list(set(transcripts_df["transcript_id"])))

    # 3. number of mapped known genes
    n_ref_genes = len(list(set(transcripts_df["ref_gene_name"])))

    # 4. number of transcripts mapped to known genes
    n_assigned = len(transcripts_df.query("ref_gene_name == ref_gene_name"))

    # 5. all transcripts that could not be assigned to existing reference genes
    n_unassigned = len(transcripts_df.query("ref_gene_name != ref_gene_name"))

    # 6. number of single exon entries

    # get only exon entries
    exons = df.query("feature == 'exon'")
    # group them according to their ref_gene_name and count how many are associated
    exons = exons.groupby(["ref_gene_name"]).count()
    # check for count 1 in any column (all show the same counts now)
    col = exons.columns[1]
    single_exons = exons.query(f"{col} == 1")
    # get total number of single exon entries
    single_exons = len(single_exons)

    results = {
        "total" : total_transcripts,
        "transcripts" : n_transcripts,
        "ref_genes" : n_ref_genes,
        "mapped" : n_assigned,
        "unmapped" : n_unassigned,
        "single_exons" : single_exons,
    }
    return results


if __name__ == "__main__":
    src = "/data/courses/rnaseq/lncRNAs/Project1/users/noah/STRINGTIE/A549_CTR/small.gtf"
    a = read_gtf(src)
    a = drop_attributes(a)
    print(a.query("reference_id == reference_id"))

