"""
This script generates a two-column index mapping transcript IDs to gene IDs based on the GFF3 file in REF_DIR

UPDATE: Nope, we just use the abundace tsv file target_id column to get the transcript ids (from which we can also get the gene ids)
        I kinda don't think that the different formatting in the gff3 file is going to work here...
        (albeit I did NOT so far test it, but it's gotta be a better shot to go with the target_ids)
        --> I think this should work nicely, since ALL run_info.json specify the exact same number of n_target,
            And since I already know that the target_id column is IDENTICAl for replicates of the same sample, there 
            is good reason to assume that ALL target_id columns are in fact identical...
"""

import os
import pandas as pd
import re 

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"

REF_TSV = os.path.join(USER_DIR, "KALLISTO", "HD1", "abundance.tsv")

REF_TSV = pd.read_table(REF_TSV, sep = "\t", comment = "#")
REF_TSV["ens_gene"] = [i.split("|")[1] for i in REF_TSV["target_id"]]
REF_TSV["ext_gene"] = [i.split("|")[5] for i in REF_TSV["target_id"]]

REF_TSV = REF_TSV[["target_id", "ens_gene", "ext_gene"]]

REF_TSV.to_csv(
    os.path.join(USER_DIR, "scripts", "r", "transcript_mapping.csv"), 
    index=False
)