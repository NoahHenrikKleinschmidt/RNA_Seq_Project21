"""
This script will assess the quality of assembly generated gtf meta-assemblies

It will parse over all entries in a given SOURCE_DIR and and generate an output csv file containing a table of stats
This will contain:
    - number of transcripts found 
    - number of distinct reference genes that could be mapped to
    - number of transcripts that were mapped 
    - number of transcripts that were mapped with only a single exon 
    - number of unmapped transcripts

Expected Arguments are: 

    1   the SOURCE_DIR containing gtf samples within cell_line specific folders.

Options are: 

   -s   File suffix (default *.gtf ) of files that are supposed to be treated. 
        
"""

import gtf 
import pandas as pd 
import os 
import sys 
import glob

args = sys.argv[1:]

# define source file
SOURCE_DIR = args[-1]
SOURCE_DIR = os.path.abspath(SOURCE_DIR)

# get suffix pattern if defined
def prepare_file_suffix(args):
    if "-s" in args: 
        sdx = args.index("-s")
        suffix = args[sdx+1]
    else: 
        suffix = "*.gtf"
    return suffix 

# get the desired gtf input files
def get_files(source_dir, args):
    suffix = prepare_file_suffix(args)
    path = os.path.join(source_dir, suffix)
    files = glob.glob(path)
    return files

all_stats = {
        "file" : [],
        "cell_line" : [], 
        "total" : [], 
        "transcripts" : [], 
        "ref_genes" : [], 
        "mapped" : [], 
        "unmapped" : [], 
        "single_exons" : [], 
    }

# iterate over all cell_lines in STRINGTIE_DIR
os.chdir(SOURCE_DIR)
for cell_line in os.listdir():

    if not os.path.isdir(cell_line):
        continue

    # get source files
    src_files = os.path.join(SOURCE_DIR, cell_line)
    src_files = get_files(src_files, args)

    for src_file in src_files:
        
        # read and generate stats
        try: 
            df = gtf.read_gtf(src_file)
        except: 
            df = gtf.read_gtf(src_file, add_header=True)
        
        # restrict only to transcripts (no exons etc...)
        stats = gtf.get_stats(df)

        # update results dict with new all_stats
        all_stats["cell_line"].append(cell_line)
        all_stats["file"].append(src_file)
        for stat in stats: 
            all_stats[stat].append(stats[stat])

# convert into dataframe and save to file 
all_stats = pd.DataFrame(all_stats)
all_stats.to_csv(os.path.join(SOURCE_DIR, "stats.csv"))


