"""
This script merges stringtie GTF files into a common merged gtf file. 
It can be used instead of stringtie --merge since this appears to loose the reference annotation...

Expected arguments are
----------------------------------------------------------------------------------------------

    $1      Parent directory where subfolders containing separate GTF files are stored in...
    $2      Formula for gtf source files (optional), default = 'same as subfolder name'
    $3      Output filename (optional), default="merged.gtf"
    $4      A python dictionary (as string, optional) speciying regex patterns of attribute
            identifiers that shall be normalised to a common nomenclature.
----------------------------------------------------------------------------------------------
"""

import pandas as pd 
import argparse
import os, glob
import re
import ast
import gtf

gtf.ATTRIBUTES = {"transcript_id" : "(ID|transcript_id|Parent)"}

args_parser = argparse.ArgumentParser(add_help=True)

args_parser.add_argument("-p", "--parent", type=str, help="Parent directory where gtf file-containing subdirectories are located.", dest = "parent", default = ".", required = True)
args_parser.add_argument("-f", "--formula", type=str, help="The formula according to which GTF files shall be selected. Default: <dirname>.gtf", default = None)
args_parser.add_argument("-o", "--outfile", type=str, help="Name of the output gtf file. Default: 'merged.gtf'", default = None)
args_parser.add_argument("-t", "--translate", type=str, help="Translate geneID vs gene_ID entries etc. to common nomenclature. Needs a python dictionary of regex patterns.", default = None)
args_parser.add_argument("-ign", "--infer_gene_name", type=bool, help="Adds a gene_name attribute to any entries that don't have a reference, where it just takes the gene_id as name. (Currently only works if 'gene_id' is the attribute identifier used!)", default = True)

args = args_parser.parse_args()

translations = ast.literal_eval(args.translate) #eval(args.translate)
translations = {i : j.replace("$", "\\") for i, j in translations.items()}


# alright, the regex substitution stuff works nicely on strings... now we need to apply it to a df
# alright, the apply is ready to go... 
# # print(translations)
# string = '"gene_id ""STRG.1""; transcript_id ""STRG.1.1""; exon_number ""1""; cov ""2.198234"";"'
# string1 = 'ID=ENST00000619216.1;geneID=ENSG00000278267.1;gene_name=MIR6859-1;coverage=0.00'

# for s in [string, string1]:
#     for i, j in translations.items():
#         s = re.sub(i, j, s)
#     print(s)


def regex_correct(string, patterns):
    for _from, _to in patterns.items():
        string = re.sub(_from, _to, string)
    return string 

# df = pd.DataFrame(dict(text = [string, string1]))

# df["conv"] = df["text"].apply(regex_correct, patterns = translations)
# print(df)

# string2 = "gene_id=STRG.1;transcript_id=STRG.1.1;"
def infer_name(string):
    try: 
        if "gene_name" in string:
            return string
        id = re.match("gene_id=([A-Z.0-9]+)", string).group(1)
        string = string + f"gene_name={id};"
        return string
    except: 
        return string

# if args.infer_gene_name:
#     s = infer_name(string2)
#     print(s)
# exit(0)


# read all possible gtf files
os.chdir(args.parent)
gtf_files = []
for i in os.listdir():

    # take default formula
    default_formula = f"{i}.gtf"
    formula = default_formula if args.formula is None else args.formula

    subdir = os.path.join(args.parent, i)
    if not os.path.isdir(subdir):
        continue

    # check for applicable gtf file
    found_files = glob.glob(os.path.join(subdir, formula))
    gtf_files.extend(found_files)

# initialize to gtf dataframe
# col_names = ["sequence_name", "source_method", "feature", "start", "stop", "score", "strand", "reading_frame", "attribute"]
# dtypes = {"sequence_name" : str, "source_method" : str, "feature" : str, "start" : int, "stop" : int, "score" : str, "strand" : str , "reading_frame" : str , "attribute" : str}

first_file = gtf_files[0]
gtf_files = gtf_files[1:]

total_gtf_df = gtf.read_gtf(first_file) # pd.read_table(first_file, sep = "\t", comment = "#", names = col_names, dtype = dtypes )

if "transcript_id" not in total_gtf_df.columns:
    raise KeyError("transcript_id is not generated!!!")

# add all other gtf files (drop duplicate lines)
for gtf_file in gtf_files:
    new_df = gtf.read_gtf(gtf_file) # pd.read_table(gtf_file, sep = "\t", comment = "#", names = col_names, dtype = dtypes )
    total_gtf_df = pd.concat([total_gtf_df, new_df]).drop_duplicates(["sequence_name", "start", "stop", "transcript_id"]).reset_index(drop=True)

# apply regex pattern correction
total_gtf_df["attribute"] = total_gtf_df["attribute"].apply(regex_correct, patterns = translations)

if args.infer_gene_name:
    total_gtf_df["attribute"] = total_gtf_df["attribute"].apply(infer_name)

total_gtf_df = total_gtf_df.drop(["transcript_id"], axis = 1)

# save to gtf file
if args.outfile is None:
    try: 
        os.mkdir(os.path.join(args.parent, "merged"))
    except: 
        pass

output_file = os.path.join(args.parent, "merged", "merged.gtf") if args.outfile is None else args.outfile
total_gtf_df.to_csv(output_file, sep="\t", header=False, index=False)



# what we want to do...
# pandas.concat([df1,df2]).drop_duplicates().reset_index(drop=True)