"""
This script now parses over all transcripts of interest and references ...
"""

import seqmap as sm
import sys
import pandas as pd
import os 

transcripts = sys.argv[1]
refs = sys.argv[2]

print("Loading ref seqs...")
# load reference TSS sequences...
ref_seqs = sm.read_fasta(refs, min_length = 30, max_length = 100)
print("ref csv saved!")

print("Loading transcript sequences...")
# load transcripts of interest (max_length some big number so we don't exclude anything...)
transcript_seqs = sm.read_fasta(transcripts, min_length = 50, max_length = 50000)
print("transcript csv saved!")

