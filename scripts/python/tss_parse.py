"""
This script now parses over all transcripts of interest and references ...
"""
# import pyximport
# pyximport.install(setup_args={"script_args" : ["--verbose"]})
import seqmap as sm
import sys
import pandas as pd
import os 

transcripts = sys.argv[1]
refs = sys.argv[2]
output_dir = "/data/courses/rnaseq/lncRNAs/Project1/users/noah/pytss_results"
basename = os.path.basename(transcripts)

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

if not os.path.exists(os.path.join(output_dir, "tmp")):
    os.mkdir(os.path.join(output_dir, "tmp"))

print("Loading ref seqs...")
# load reference TSS sequences...
if not os.path.exists(f"{refs}.csv"):   
    ref_seqs = sm.read_fasta(refs, min_length = 30, max_length = 100)
else:
    ref_seqs = pd.read_table(f"{refs}.csv", sep = "\t", names = ["id", "seq"])

print("Loading transcript sequences...")
# # load transcripts of interest (max_length some big number so we don't exclude anything...)
# if not os.path.exists(f"{transcripts}.csv"):
#     transcript_seqs = sm.read_fasta(transcripts, min_length = 50, max_length = 100000)
# else:
#     transcript_seqs = pd.read_table(f"{transcripts}.csv", sep = "\t", names = ["id", "seq"])

# --------------------------------------------------------------------------------
# We modify this part because we now split the already ordered csv file 
# into subfiles, so no need to read a fasta here...
# --------------------------------------------------------------------------------
transcript_seqs = pd.read_table(f"{transcripts}", sep = "\t", names = ["id", "seq"])
# note we have to add the header manually since the table exported from read_fasta does not include any header...

print("Setting up Mapper...")
# setup the mapper
mapper = sm.Mapper(transcript_seqs, ref_seqs)
print("Start parsing...")
mapper.parse(savemode = True, tmpfile = f"{output_dir}/tmp/{basename}")
print("Done parsing...")

# save results
print("Saving results...")
results = mapper.get()
results["score"] = [list(i) for i in results["score"]] # convert to list, otherwise we dont get values when storing to file...
results.to_csv(
                f"{output_dir}/{basename}_tss.csv", 
                sep = "\t", 
                index = False
            )
print("All done...")