from setuptools import setup, Extension
# from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy as np

package = Extension('seqmap', ['seqmap.pyx'], include_dirs=[np.get_include()])
setup(
    ext_modules = cythonize([package]),
)