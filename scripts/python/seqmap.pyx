"""
This module defines the class Mapper that takes in a dataframe of transcript sequences of interest
and a corresponding dataframe of reference sequences to map onto. 
It makes use of Smith-Waterman local alignment and maps the alignment scores onto the overall sequence.
It ultimately saves a dataframe of scores mapped over the entire sequence, which can be visualised as a heatmap.
"""

import pandas as pd
import numpy as np
import subprocess 
import os 
import skbio
import cython
cimport numpy as np
cimport cython

def read_fasta(filename, min_length = 10, max_length = 100, drop_duplicates = True, save = True):
    """
    Reads a Fasta File and reduces it to sequences of max_length => length >= min_length
    """
    tmpfile = "{}.tmp".format(filename)
    subprocess.run("echo 'id\tseq' > {}.table".format(tmpfile), shell = True)

    # get all headers and sequences
    subprocess.run("grep -Eo '[ATGC]{2,}' " + " {} > {}.seq".format(filename, tmpfile), shell =True)
    subprocess.run("grep -Eo '>(.+)' " + " {} > {}.name".format(filename, tmpfile), shell =True)

    # merge the two files...
    subprocess.run("paste {tmpfile}.name {tmpfile}.seq >> {tmpfile}.table".format(tmpfile = tmpfile), shell = True)
    
    tss_df = pd.read_csv("{}.table".format(tmpfile), sep = "\t")
    os.remove("{}.seq".format(tmpfile))
    os.remove("{}.name".format(tmpfile))
    os.remove("{}.table".format(tmpfile))

    # filter for sequence size to be in the specified range
    tss_df = tss_df[
                        [
                            max_length >= len(i) >= min_length for i in tss_df["seq"]
                        ]
                    ] 
                    
    tss_df = tss_df.dropna()
    if drop_duplicates:
        tss_df = tss_df.drop_duplicates(["seq"])
    if save:
        tss_df.to_csv("{}.csv".format(filename), sep = "\t", index = False, header = False)
    return tss_df

def buffer_gtf(filename, pre_buffer = 300, post_buffer = 200, save=True):
    """
    Extends the coordinates of start and stop for each feature (line) by the pre and post buffers...
    """
    names = ["chrom", "source", "feature", "start", "stop", "score", "strand", "frame", "attribute"]
    df = pd.read_table(filename, sep = "\t", names = names)
    df["start"] = df["start"] - pre_buffer
    df["stop"] = df["stop"] + post_buffer

    # reset indices to 1 in case buffering made them negative
    df["start"][df["start"] <= 0] = 1

    if save: 
        newfile = filename.replace(".gtf", "_buffered.gtf")
        df.to_csv(newfile, sep = "\t", index = False, header=False)
    return df


# The idea:
# We initialise for each seq (transcript) a new array for its positions
# and one for its potential tss sites. then we let each tss align and get the postiion alongside the identity 
# of the match. we will only look at the best match, hence we will only align each tss once to the seq
# and we will then gradually acumulate signals for tss...

class Mapper(object):
    """
    This class takes in a dataframe from reading a fasta file of sample transcripts and another dataframe of reference sequences to align to
    """
    def __init__(self, target_seqs, ref_seqs):
        self._data = target_seqs
        self._ref_seqs = ref_seqs


    def get(self):
        """
        Returns the stored data
        """
        df = self._data
        df = df.drop(columns = ["dna_seqs"]) 
        return df

    def parse(self, savemode = False, tmpfile = None):
        """
        Parses the ref sequences given and aligns each, storing the aligned identities for each transcript in a dataframe
        If in savemode the data will be saved after each 10 iterations (if this is set, a location for the saved files as to be set).
        """
        
        # predeclare the empty score vectors
        self._data["length"] = self._data["seq"].apply(len)
        self._data["score"] = self._data["length"].apply(np.zeros)
        
        # already convert sequence strings into the necessary skbio.DNA format...
        self._data["dna_seqs"] = self._data["seq"].apply(skbio.DNA)
        self._ref_seqs["dna_seqs"] = self._ref_seqs["seq"].apply(skbio.DNA)

        idx = 0
        for seq, score_vector in zip(self._data["dna_seqs"], self._data["score"]):
              
            print("Reading entry: {}".format(idx))
            for ref_t in self._ref_seqs["dna_seqs"]:
                alignment = skbio.alignment.local_pairwise_align_ssw(seq, ref_t)
                start, stop = alignment[2][0]
                score = alignment[1]
    
                # update the scores
                score_vector[slice(start, stop)] += score
            idx += 1
            if savemode and idx%10 == 0: 
                df = self._data[["id", "length", "score"]]
                df["score"] = df["score"].apply(list)
                df.to_csv("{}.tmp".format(tmpfile), header = False, index = False)
    

    # not used currently, just a theoretical improvement that needs more testing...
    def parse1(self):
        """
        Parses the ref sequences given and aligns each, storing the aligned identities for each transcript in a dataframe
        """

        # predeclare the empty score vectors
        self._data["length"] = self._data["seq"].apply(len)
        self._data["score"] = self._data["length"].apply(lambda x: np.zeros(x))
        
        # already convert sequence strings into the necessary skbio.DNA format...
        self._data["dna_seqs"] = self._data["seq"].apply(skbio.DNA)
        self._ref_seqs["dna_seqs"] = self._ref_seqs["seq"].apply(skbio.DNA)

        idx = 0

        # we have to iterate over the refrence tss here... if we want to use a more apply, apply, etc approach...
        for ref_t in self._ref_seqs["dna_seqs"]:

            self._data["alignment"] = list(map(lambda x: single_align(seq = x, ref = ref_t), self._data["dna_seqs"]))
            self._data["start"] = self._data["alignment"].apply(lambda x: x[2][0][0])
            self._data["stop"] = self._data["alignment"].apply(lambda x: x[2][0][1])
            self._data["alscore"] = self._data["alignment"].apply(lambda x: x[1])

            for i in self._data.iterrows(): 
                i[1]["score"][slice(i[1]["start"], i[1]["stop"])] += i[1]["alscore"]        
        
            idx += 1
            if idx == 3:  # remove when using on real data.. (just for testing)
                break

        # for seq, score_vector in zip(self._data["dna_seqs"], self._data["score"]):
            
        #     self._data["alignment"] = list(map(lambda x: single_align(seq = seq), self._ref_seqs["dna_seqs"]))
            
            
        #     print("Reading entry: {}".format(idx))
        #     for ref_t in self._ref_seqs["dna_seqs"]:
        #         alignment = skbio.alignment.local_pairwise_align_ssw(seq, ref_t)
        #         start, stop = alignment[2][0]
        #         score = alignment[1]
    
        #         # update the scores
        #         score_vector[slice(start, stop)] += score


def single_align(seq, ref):
    alignment = skbio.alignment.local_pairwise_align_ssw(seq, ref)
    return alignment
    


def single_align(seq, ref):
    alignment = skbio.alignment.local_pairwise_align_ssw(seq, ref)
    return alignment
