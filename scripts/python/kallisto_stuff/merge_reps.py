"""
This script is designed to merge the HD1, 2 ... individually generated results from 
KALLISTO with the results produced from the grouped KALLISTO application.

The goal is to maintain information about the spread and deviation of each detected feature over all replicates
so as not to have to rely on one single TPM count (which is presumably averaged by KALLISTO?). 

Like this, we will be able to compare features between sample conditions for instance by a T-Test 
to check for significance in terms of difference...

It will probably not be able to replace Sleuth analysis, 
but would be very cool to compare hits with "common-method" results.

Steps we implement:

- We write a class to read a merged (grouped) source file --> stores a df 
- It also grabs the information about the associated replicates (from replicates.csv)
- Then it has a parser to grab the associated tpm columns and adds them to the source_df
    - it also vets if the columns are also truly identical...
- and ultimately it stores a new merged df. 

NOTE: As currently, this approach is NOT followed up, as I try to use Sleuth (I figured out that sleuth is doing some serious math stuff, so I'm not going to re-implement this any time soon...)
"""

import pandas as pd 
import os, glob 
import matplotlib.pyplot as plt
import seaborn as sns
from statistics import mean 
import plotly
from plotly.subplots import make_subplots
import plotly.graph_objects as go

USER_DIR = "/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR = os.path.join(USER_DIR , "reads")
REPLICATES = os.path.join(USER_DIR, "replicates.csv")

# will have to be adjusted for actual application...

TEST_DIR = os.path.join(USER_DIR , "test")

GROUPED_DIR = os.path.join(USER_DIR , "KALLISTO_grouped")
UNGROUPED_DIR = os.path.join(USER_DIR , "KALLISTO")


def read_replicates(filename:str) -> pd.DataFrame:
    """
    Reads the replicates.csv file and gets the groups and associated replciates in a dataframe
    """
    replicates = pd.read_csv(filename, sep = ";", names = ["Sample", "Replicates"])
    replicates["Replicates"] = [i.split(" ") for i in replicates["Replicates"]]
    return replicates
    

class AbundanceReader:
    """
    Reads an abundance files and merges replicates for a given group / sample condition.
    """
    def __init__(self) -> None:
        self._SOURCE = None
        self._REPLICATES = None
        self._df = None
        self._replicates_dfs = []
        self._source_df = None
        self._all_sources = read_replicates(REPLICATES)
        self._save_to = None
        self._grouped_loc = None
        self._ungrouped_loc = None
    
    def setup(self, save_to, grouped, ungrouped):
        """
        Setup directories for grouped and ungrouped samples
        """
        self._save_to = save_to
        self._grouped_loc = grouped
        self._ungrouped_loc = ungrouped

    def save(self, use_subfolders=False):
        """
        Saves the dataframe to a new file
        """
        save_loc = os.path.join(self._save_to, f"{self._SOURCE}_merged.tsv") if not use_subfolders else os.path.join(self._save_to, self._SOURCE, f"{self._SOURCE}_merged.tsv")
        self._df.to_csv(save_loc, index = False, sep = "\t")

    def get(self):
        """
        Returns the merged dataframe
        """
        return self._df

    def source(self, sample):
        """
        Links the Sample group to be used
        """
        self._SOURCE = sample

        # read the source_df
        source_loc = os.path.join(self._grouped_loc, self._SOURCE, "abundance.tsv")
        self._source_df = self._read_abundance_file(source_loc)
       
        # setup replicates for sample
        sample_reps = self._all_sources.query(f"Sample == '{self._SOURCE}'")["Replicates"]
        sample_reps = list(*sample_reps)
        if sample_reps == []:
            raise Warning("Unknown sample: {sample}, could not be found in replicates.csv".format(sample = sample))
        self._REPLICATES = sample_reps
        
        # read replicate dfs 
        if self._replicates_dfs != []: self._replicates_dfs = []
        for replicate in self._REPLICATES:
            df = self._get_abundance_file(replicate)
            self._replicates_dfs.append(df)
    

    def merge(self):
        """
        Merges the abundance.tsv of sample and associated replicates. 
        Generates a new dataframe, where each replicate tpm column was added as a new column...
        """
        if self._SOURCE is None:
            raise Warning("No source sample has been assigned yet! Make sure to link a source and that replicate folders are in your designated directory.")

        # vet that ALL dfs have the SAME target_IDs in them in the same order
        for replicate in self._replicates_dfs:
            if not all(replicate["target_id"] == self._source_df["target_id"]):
                raise Warning("Replicate and merged dataframes show different indices!")

        new_df = self._source_df
        idx = 0 
        for replicate in self._replicates_dfs:
            subset = replicate[["target_id", "tpm"]]
            colname = f"rep_{idx+1}"
            subset = subset.rename(columns = {"tpm" : colname})
            new_df = new_df.merge(subset, on = "target_id")
            idx += 1
        
        # store merged df
        self._df = new_df

    def _get_abundance_file(self, parent):
        """
        Finds the abundace.tsv file for a replicate parent (like HD1)
        """
        abundance_loc = os.path.join(self._ungrouped_loc, parent, "abundance.tsv")
        if os.path.exists(abundance_loc):
            return self._read_abundance_file(abundance_loc)
        else:
            raise Warning("Could not find abundance file for replicate parent")

        
    def _read_abundance_file(self, filename):
        """
        Reads an abundance.tsv file into a pandas DataFrame, 
        and stores it into self._replicates_dfs
        """
        df = pd.read_table(filename, sep = "\t")
        return df 
    

    def generate_stats(self):
        """
        Will compute an average and standard deviation column from rep_1, ... columns
        It will comput the following columns:
        avg - mean of rep columns
        stdv - standard deviation of rep columns
        diff_abs - absolute difference between tpm and avg columns
        diff_rel - relative difference between tpm and avg columns (diff_abs / mean(avg, tpm))
        """
        reps = [i for i in self._df.columns if "rep" in i]
        reps = self._df[reps]
        self._df["avg"] = reps.mean(axis = 1)
        self._df["stdv"] = reps.std(axis = 1)
        self._df["diff_abs"] = self._df["tpm"] - self._df["avg"]
        self._df["diff_rel"] = self._df["tpm"] / self._df["avg"]

    def summary(self, save = True):
        """
        Generates summary figures for tpm and avg of merged datasets
        """
        self._static_summary(save)
        self._dynamic_summary(save)
    
    def _dynamic_summary(self, save = True):
        """
        Generates a dynamic (plotly) summary figure
        """
        fig = make_subplots(rows=2, cols=2, 
                            subplot_titles =["Absolute Difference (tpm - avg)", "avg vs tpm", "Relative Difference (tpm / avg)", "avg vs tpm"], 
                            
                            )

        fig.add_trace(
            go.Scatter(
                    
                    #name = self._df["diff_abs"],
                    x=self._df["target_id"], y=self._df["diff_abs"], 
                    hoverinfo = "x+y",

            ),
        row = 1, col = 1
        )

        fig.update_xaxes(showticklabels=False, row=1, col=1)

        fig.add_trace(
            go.Scatter(
                    
                    # name = self._df["diff_rel"],
                    x=self._df["target_id"], y=self._df["diff_rel"], 
                    hoverinfo = "name",
                
            ),
        row = 2, col = 1
        )
        fig.update_xaxes(showticklabels=False, row=2, col=1)
        
        fig.add_trace(
            go.Scatter(
                        
                        text = self._df["diff_abs"],
                        x=self._df["avg"], y=self._df["tpm"], 
                        marker=dict(
                            opacity = 0.3, 
                            color=self._df["diff_abs"],
                            cmax=max(self._df["diff_abs"]),
                            cmin=min(self._df["diff_abs"]),
                            # colorbar=dict(
                            #     title="Absolute Difference"
                            # ),
                            colorscale="Viridis"
                        ),
                        mode = "markers",
                        hoverinfo = "text",
                    ),
            row=1, col=2
        )

        fig.add_trace(
            go.Scatter(
                        
                        text = self._df["diff_rel"],
                        x=self._df["avg"], y=self._df["tpm"], 
                        marker=dict(
                            opacity = 0.3, 
                            color=self._df["diff_rel"],
                            cmax=max(self._df["diff_rel"]),
                            cmin=min(self._df["diff_rel"]),
                            # colorbar=dict(
                            #     title="Relative Difference", 

                            # ),
                            colorscale="Viridis"
                        ),
                        mode = "markers",
                        hoverinfo = "text",
                    ),
            row=2, col=2
        )

        fig.update_layout(height=600, width=800, title_text=f"summary_{self._SOURCE}")
        if save:
            fig.write_html(os.path.join(self._save_to, f"summary_{self._SOURCE}.html"))
        return fig 

    def _static_summary(self, save = True):
        """
        Generates a summary figure showing the differences between rep-avg and tpm columns.
        """
        if "avg" not in self._df.columns:
            self.generate_stats()

        fig, axs = plt.subplots(ncols = 2, nrows = 2)
        
        # line for scatterplot (would indicate 1:1 correlation)
        line = list(range(int(max(self._df["tpm"]))))


        # plot absolute differences as line chart and scatterplot avg vs tpm
        self._df.plot(
                        x = "target_id", y = "diff_abs", 
                        ax = axs[0, 0],
                        xlabel = "",   xticks = [], 
                        ylabel = "Absolute Difference",
                        title = "tpm - avg", 
                        legend = False
                    )
        self._df.plot.scatter(
                                x = "avg", y = "tpm", 
                                c = "diff_abs", cmap = "coolwarm", 
                                ax = axs[0, 1], 
                                xlabel = "", title = "avg vs tpm",
                                alpha = 0.5,
                            )
        axs[0,1].plot(line, line, c = "gray", linestyle = "--", alpha = 0.1)
        
        # plot relative differences as line chart and scatterplot avg vs tpm
        self._df.plot(
                        x = "target_id", y = "diff_rel", 
                        ax = axs[1, 0],
                        xticks = [], 
                        ylabel = "Relative Difference",
                        title = "tpm / avg", 
                        legend = False
                    )

        self._df.plot.scatter(
                                x = "avg", y = "tpm", 
                                c = "diff_rel", cmap = "coolwarm", 
                                ax = axs[1, 1], 
                                alpha = 0.5,
                            )

        axs[1,1].plot(line, line, c = "gray", linestyle = "--", alpha = 0.1)

        # add title
        fig.suptitle(f"Summary of: {self._SOURCE}" , fontsize = 15, weight = "bold")
        
        plt.subplots_adjust(bottom=0.1, left = 0.12, right=0.91, top=0.85, hspace= 0.2, wspace= 0.3)        
        
        if save:
            plt.savefig(os.path.join(self._save_to, f"summary_{self._SOURCE}.png"))
        
        return fig 



if __name__ == "__main__": 

    testmode = True

    if testmode:
        
        TEST_DIR = os.path.join(USER_DIR , "test")

        GROUPED_DIR = os.path.join(TEST_DIR , "KALLISTO_grouped")
        UNGROUPED_DIR = os.path.join(TEST_DIR , "KALLISTO_ungrouped")
        print(TEST_DIR, GROUPED_DIR, UNGROUPED_DIR)
        reader = AbundanceReader()
        reader.setup(
            save_to = os.path.join(TEST_DIR), 
            grouped = GROUPED_DIR,
            ungrouped = UNGROUPED_DIR,
            )


        replicates = read_replicates(REPLICATES)
        r = replicates["Sample"][0]
        
        reader.source(r)
        reader.merge()
        reader.summary()

    else:
                
        MERGED_DIR = os.path.join(USER_DIR, "KALLISTO_merged")

        reader = AbundanceReader()
        reader.setup(
            save_to = MERGED_DIR, 
            grouped = GROUPED_DIR,
            ungrouped = UNGROUPED_DIR,
            )

        replicates = read_replicates(REPLICATES)
        for r in replicates["Sample"]:
            print("Reading", r, "...")
            reader.source(r)
            reader.merge()
            reader.summary()
            reader.save()

