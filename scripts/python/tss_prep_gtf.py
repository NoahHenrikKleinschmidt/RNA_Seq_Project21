"""
This script introduces buffers around the Novel transcript regions
"""

import seqmap as sm
import sys

file_to_buffer = sys.argv[1]

pre = 500
post = 500

sm.buffer_gtf(file_to_buffer, pre, post)