#!/bin/bash

# This is step 1.2: 
# We perform quality control of all our reads using FastQC and MultiQC 
#
# This is the wrapper script for single_QC 
# it can be executed normally in BASH


USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads


for i in $(ls $READS_DIR); do 
    
    sbatch $USER_DIR/scripts/single_QC.slurm $i

done
