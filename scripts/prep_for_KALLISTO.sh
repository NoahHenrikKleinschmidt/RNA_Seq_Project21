#!/bin/bash

# This is step 4.1: 
# We perpare to quantify our transcriptomes using kallisto. 
# Kallisto uses pseudo-alignment to the reference genome (independently from what STAR has mapped). 
# It will generate tables of expression levels for different transcript isoforms. 
# We could also use STAR modeCount to simply count the mapped instances, but this will not give us isoform overview, or statistical assessment of the results. 
#
# NOTE: This is the wrapper that calls the index_KALLISTO.slurm script.
# NOTE: This preparatory step is required as kallisto requires an indexed genome...
#       It does not need to be repeated...
# 

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads

KALLISTO_DIR=$USER_DIR/KALLISTO
# make a new KALLISTO_DIR within user_home
if [[ ! -d $KALLISTO_DIR ]]; then 
    mkdir $KALLISTO_DIR

fi

# get copy of reference genome into KALLISTO_DIR
# and index reference genome if not done already
KALLISTO_REF_DIR=$KALLISTO_DIR/ref
if [[ ! -d $KALLISTO_REF_DIR ]]; then 
    mkdir -p $KALLISTO_REF_DIR  
    cd $KALLISTO_REF_DIR

    sbatch $USER_DIR/scripts/index_KALLISTO.slurm 

fi 
