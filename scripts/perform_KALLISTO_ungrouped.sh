#!/bin/bash

# This is step 4.2: 
# We now actually do quantify our transcriptomes using kallisto. 
# Kallisto uses pseudo-alignment to the reference genome (independently from what STAR has mapped). 
# It will generate tables of expression levels for different transcript isoforms. 
# We could also use STAR modeCount to simply count the mapped instances, but this will not give us isoform overview, or statistical assessment of the results. 
#
# NOTE: This is the wrapper that calls single_KALLISTO.slurm
# 
# NOTE: This version of the script will use the individual HD1 ... 
#       samples separately and NOT merge them into CTR etc. groups!
# 

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads

REPLICATES=$USER_DIR/replicates.csv

OUTPUT_DIR=$USER_DIR/KALLISTO

if [[ ! -d $OUTPUT_DIR ]]; then 
    mkdir $OUTPUT_DIR
fi 

cd $READS_DIR

for sample in $(ls); do 

    if [[ "$sample" =~ slurm.+ ]]; then
        continue
    fi

    echo "======================================================"
    echo "Sample: $sample"
    

    pair_L1=$sample/raw/*L1*
    pair_L2=$sample/raw/*L2*
    all_samples=$(echo "${pair_L1} ${pair_L2}")

    # call on single_kallisto 
    # (NOTE: the "$all_samples" is important, otherwise ( like just $all_samples ) 
    #        only the first element is being recognised by single_KALLISTO!) 
    sbatch $USER_DIR/scripts/single_KALLISTO.slurm "$sample" "$all_samples"
    

done

echo "======================================================"