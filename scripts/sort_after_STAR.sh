#!/bin/bash

# This is step 4.5: 
# We sort the results of STAR mapping into the designated subfolders within STAR. 
# Technically, this should have happened already in step 4, but this did not seem 
# to have worked properly. So we add this (maybe optional) step now.
#




USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads
STAR_DIR=$USER_DIR/STAR


cd $READS_DIR

for cell_line in $(ls); do 

    cd $STAR_DIR

    STAR_cell_line=$STAR_DIR/$cell_line

    # move the Aligned Log and SJ.out files into the respective subfolder
    mv $(ls ${cell_line}A*) $STAR_cell_line
    mv $(ls ${cell_line}L*) $STAR_cell_line
    mv $(ls ${cell_line}S*) $STAR_cell_line

done