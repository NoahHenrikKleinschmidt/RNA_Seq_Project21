#!/bin/bash

# This is step 6.3.1: 
# We grep the original transcript IDs from the GTF file so we can re-assemble the fasta loci with the original transcript IDs...
#
# NOTE: This is the actual script...
#
# Expected aruments are: 
# ----------------------------------------------------------------
#
#       $1          A GTF file
#
# ----------------------------------------------------------------
#

#SBATCH --mail-type=end,fail
#SBATCH --mail-user=noah.kleinschmidt@students.unibe.ch
#SBATCH --job-name="gtf_transcript_ids"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --time=00:02:00
#SBATCH --mem-per-cpu=1G

file="$1"

awk '{print ">transcript::" $1 ":" $4 -1 "-" $5 "," $12}' "$file" | sed -E 's/""//g' | sed 's/;//' > "$file".ids.csv