#!/bin/bash

# This is step 1.3: 
# We performed quality control of all our reads using MultiQC 
#
# Now we will download all our MultiQC results to our local drive
# since we have to do this from the local terminal 
# we make this script to collect all results zip files into one 
# directory which can be downloaded at once...


USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads

# source the important module stuff from Bioinformatics Practical (just in case we need it)
source /home/noahkleinschmidt/scripts/modules.sh


cd $USER_DIR
MULTIQC_DIR=$USER_DIR/MultiQC

if [[ ! -d $MULTIQC_DIR ]]; then 
    mkdir $MULTIQC_DIR
fi


cd $READS_DIR
for cell_line in $(ls); do
    
    # hardlink all named reports in cell_line/multiqc folders tin MULTIQC dir
    report=$(ls $cell_line/multiqc/*$cell_line*.html)
    ln -s $READS_DIR/$report $MULTIQC_DIR/MultiQC_$cell_line.html
done