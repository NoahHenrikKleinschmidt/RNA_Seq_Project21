#!/bin/bash

# This is step 4.3: 
# We now actually do quantify our transcriptomes, but with HTSeq.
# We have to do this, since Kallisto is unable to work with un-annotated transcripts. 
# Hence, we perform HTSeq quantification, followed by DESeq analysis to check for un-annotated transcripts as well...  
#
# NOTE: This is the wrapper that calls the actual slurm script.
# 

#SBATCH --mail-type=end,fail
#SBATCH --mail-user=noah.kleinschmidt@students.unibe.ch
#SBATCH --job-name="HTSseq"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --time=10:00:00
#SBATCH --mem-per-cpu=10G

# load the module
module add UHTS/Analysis/HTSeq/0.9.1

REF_DIR="/data/courses/rnaseq/lncRNAs/Project1/references"
USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
STAR_DIR=$USER_DIR/STAR_ungrouped

REPLICATES=$USER_DIR/replicates.csv

HT_SEQ_DIR=$USER_DIR/HTSEQ

if [[ ! -d $HT_SEQ_DIR ]]; then

    mkdir $HT_SEQ_DIR

fi


cd $STAR_DIR

# remove residual slurm outputs
noslurm

for i in $(ls); do 

    if [[ ! -d $i ]]; then continue; fi

    # make output directory...
    if [[ ! -d $HT_SEQ_DIR/$i ]]; then

        mkdir $HT_SEQ_DIR/$i
                    
    fi

    # call single_HTSEQ
    sbatch $USER_DIR/scripts/single_HTSEQ.slurm $i

done


