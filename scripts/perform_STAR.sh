#!/bin/bash

# This is step 2.2: 
# We map our reads to the reference genome using STAR
#
# NOTE: This is the wrapper that will call on single_map.slurm
#
#
# This script will perform mapping of a reads pair to the reference 
# genome using STAR. It will do so on both grouped samples (merging HD1 2 3 to generate one BAM file for A549_CTR)
# as well as on individual samples (generating a HD1.bam etc. separately)
#
#


USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads
STAR_DIR=$USER_DIR/STAR

cd $USER_DIR

# if not yet present make STAR_DIR
if [[ ! -d $STAR_DIR ]]; then 
    mkdir $STAR_DIR
fi



REPLICATES=$USER_DIR/replicates.csv

# read the replicates information and assign sample names and get corresponding cell_line IDs
while read line; do 

    sample_name=$(echo $line | awk -F ';' '{print $1}')
    cell_lines=$(echo $line | awk -F ';' '{print $2}')

    # get for all specified samples the replicates' L1 and L2 reads and concatenate in a list
    all_samples=""
    reads_R1=""
    reads_R2=""
    for cell_line in $cell_lines; do
        R1=$(echo $(ls $READS_DIR/$cell_line/raw/*R1*.fastq.gz))
        R2=$(echo $(ls $READS_DIR/$cell_line/raw/*R2*.fastq.gz))
        reads_R1=$(echo "${reads_R1} ${R1}")
        reads_R2=$(echo "${reads_R2} ${R2}")
    done

    # trim and assemble R1 and R2 reads into all_samples
    reads_R1=$(echo "${reads_R1}" | xargs | tr " " "," )
    reads_R2=$(echo "${reads_R2}" | xargs | tr " " "," )
    all_samples=$(echo "${reads_R1} ${reads_R2}")


    echo "======================================================"
    echo "Sample: $sample_name"
    # echo "Files: $all_samples"
    c=0
    for i in $all_samples; do let c=$c+1; done; echo "Number of fastq input files: $c"

    # call single_STAR
    sbatch $USER_DIR/scripts/single_STAR.slurm $sample_name "$all_samples" $STAR_DIR

done < $REPLICATES

echo "======================================================"

# since DESEQ will need multiple count files per cell line we
# will also perform alignment on all HD1 ... separately again...

cd $READS_DIR
for cell_line in $(ls); do 

    if [[ ! -d $cell_line ]]; then continue; fi

    # get for all specified samples the replicates' L1 and L2 reads and concatenate in a list
    all_samples=""
    reads_R1=""
    reads_R2=""

    R1=$(echo $(ls $READS_DIR/$cell_line/raw/*R1*.fastq.gz))
    R2=$(echo $(ls $READS_DIR/$cell_line/raw/*R2*.fastq.gz))
    reads_R1=$(echo "${reads_R1} ${R1}")
    reads_R2=$(echo "${reads_R2} ${R2}")


    # trim and assemble R1 and R2 reads into all_samples
    reads_R1=$(echo "${reads_R1}" | xargs | tr " " "," )
    reads_R2=$(echo "${reads_R2}" | xargs | tr " " "," )
    all_samples=$(echo "${reads_R1} ${reads_R2}")

    echo "======================================================"
    echo "Sample: $cell_line"
    # echo "Files: $all_samples"
    c=0
    for i in $all_samples; do let c=$c+1; done; echo "Number of fastq input files: $c"

    # call single_STAR                                 # save output to STAR_prev
    sbatch $USER_DIR/scripts/single_STAR.slurm "$cell_line" "$all_samples" "$USER_DIR/STAR_ungrouped"

done