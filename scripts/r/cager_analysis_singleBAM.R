# This script performs CAGEr analysis on the grouped BAM files from STAR_grouped
# Since ESAT is probably not going to work, we'll have to do this...

library(BiocManager)
# BiocManager::install("CAGEr")

library(CAGEr)
library(rtracklayer)
library(BSgenome.Hsapiens.UCSC.hg38)

library(devtools)
library(stringr) 
# load_all("CAGEr")



# possibly conversion step from bam to ctss prior to starting CAGEexp...
ref_dir = file.path("/data/courses/rnaseq/lncRNAs/Project1/references")
user_dir = file.path("/data/courses/rnaseq/lncRNAs/Project1/users/noah")
# source_dir = file.path(user_dir, "STAR/_gathered")
# we'll just try out one BAM file
source_dir = file.path(user_dir, "STAR_ungrouped/HD1")

output_dir = file.path(user_dir, "cager_results")

input_files = list.files(source_dir, full = TRUE)

condition_labels = c()
for (i in input_files) { condition_labels = append(condition_labels, str_replace(basename(i), ".bam", ""))}

# reference GFF Annotation
annotation_file = file.path(ref_dir, "gencode.v38.annotation.gff3")
gff_annotation = import.gff(annotation_file, genome = "hg38")


for (file in seq_along(input_files)) {

        input_file = input_files[file]
        label = condition_labels[file]

        # # generic setup of CageEXP
        cage <- CAGEexp( 
                        genomeName = "BSgenome.Hsapiens.UCSC.hg38",
                        # we will use the grouped data, so one input file each... 
                        # (hopefully it won't complain...)
                        inputFiles     = input_file,
                        inputFilesType = "bamPairedEnd",
                        # labels will be A549_CTR etc...
                        sampleLabels   = label
                )

        # # reading in the data
        getCTSS(
                cage, 
                sequencingQualityThreshold = 5,
                mappingQualityThreshold = 10,
                # since apparently this is not yet supproted for CAGEexp
                correctSystematicG = FALSE, 
                removeFirstG = FALSE
                )

        # normalise to TPM
        normalizeTagCount(cage, method = "simpleTpm")

        # # count expression numbers per chromosome and performs normalisation
        # # I don't think I need this ...
        # # summariseChrExpr(exampleCAGEexp)

        # # annotate CTSS from the data, requires a reference annotation file...
        annotateCTSS(cage, gff_annotation)


        raw_tss_counts = CTSStagCount(cage)

        # # I think these two are not necessary for this...
        # # CTSStoGenes(exampleCAGEexp)
        raw_clusters = clusterCTSS(
                                object = cage, 
                                threshold = 1, nrPassThreshold = 1,
                                thresholdIsTpm = TRUE, 
                                method = "distclu",
                                maxDist = 20, 
                                removeSingletons = FALSE, keepSingletonsAbove = 1,
                                maxLength = 500, 
                                reduceToNonoverlapping = TRUE,
                                )

        consensus_clusters = consensusClusters(cage)

        # save results 
        raw_filename = file.path(output_dir, "raw_tss_counts.csv")
        write.csv(as.data.frame(raw_tss_counts), file=raw_filename)

        raw_cluster_filename = file.path(output_dir, "raw_tss_cluster_counts.csv")
        write.csv(as.data.frame(raw_clusters), file=raw_cluster_filename)

        con_cluster_filename = file.path(output_dir, "concensus_tss_counts.csv")
        write.csv(as.data.frame(consensus_clusters), file=con_cluster_filename)
}