# An auxiliary module that contains small helper functions ...

split_samples = function(metadata, by, named=TRUE) {
    # """
    # This function splits a sample metadata dataframe according to unique valued-subsets according to the by-column (split by the values in that column...)
    # It returns a tuple of subset dataframes.
    # """
    splitted_keys = metadata[by]
    splitted_keys = splitted_keys[!duplicated(splitted_keys),]
    
    subsets = list()
    i = 1
    for (key in splitted_keys) {

        subset = metadata[by] == key
        subset = metadata[subset,]
        
        if (!named) { k = i } else { k = key }
        subsets[[k]] = data.frame(subset)
        i = i + 1
    }

    return(subsets)
}

split_wt_conditions = function(metadata, column, ref) {
    # This function gets all conditions found within the metadata subset of a given cell-line
    # it returns a dataframe of codes and corresponding names
    # Note that column means the named column, it is assumed that the encoded column is named c_column (where "colunm" is just whatever the named column is named...)

# c_column = paste0("c_", column)

# tmp = metadata#[!(metadata$c_column == 0),]
tmp = metadata[column]
tmp = tmp[!duplicated(tmp),]
tmp = tmp[tmp != ref]
return(tmp)

}