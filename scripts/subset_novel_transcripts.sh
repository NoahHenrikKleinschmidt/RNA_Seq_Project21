#!/bin/bash

# This is step 6.1: 
#  Here we check for the number of novel transcripts identified. 
# We use a GTF file as input and extract all transcript entries that have no geneID reference to it... 
#
#  Expected arguments are
# ----------------------------
#
#   $1      A GTF File
#
# ----------------------------
# 


#SBATCH --mail-type=end,fail
#SBATCH --mail-user=noah.kleinschmidt@students.unibe.ch
#SBATCH --job-name="novel"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --time=00:20:00
#SBATCH --mem-per-cpu=10G

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
STRINGTIE_DIR=$USER_DIR/STRINGTIE

INPUT_FILE=$1
OUTPUT_FILE="${INPUT_FILE}_non_annoatated.gtf"

cat "$INPUT_FILE" | awk '{if ($3=="transcript") {print $0}}' | grep -vE "(ref_gene_id|gene_name)" > $OUTPUT_FILE


# legacy stuff from when the script did stats on novel transcripts (now part of get_STRINGtie_stats... )

# OUTPUT_FILE=$STRINGTIE_DIR/novel_stats.csv
# echo "Sample,Total,Annotated" > $OUTPUT_FILE

# TMPFILE=$STRINGTIE_DIR/_tmp.txt
# cd $STRINGTIE_DIR
# for i in $(ls); do 

#     if [[ ! -d $i ]]; then continue; fi

#     # get all transcripts 
#     # read gtf      # get all "transcript" lines
#     cat $i/$i.gtf | awk '{if($3=="transcript") {print $0}}' > $TMPFILE

#     # get all annotated transcripts
#     all_annotated=$(cat $TMPFILE | grep -c "reference_id")

#     # store all un-annotated transcripts as new_gtf
#     cat $TMPFILE | grep --invert-match "reference_id" > "$i/${i}_non_annotated_transcripts.gtf"

#     # get total length
#     total=$(cat $TMPFILE | grep -c .)

#     result="$i,$total,$all_annotated" 
#     echo $result >> $OUTPUT_FILE

#     # reset TMPFILE
#     rm $TMPFILE

# done