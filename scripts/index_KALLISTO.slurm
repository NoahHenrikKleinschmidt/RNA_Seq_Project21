#!/bin/bash

# This is step 4.1.1: 
# We prepare to quantify our transcriptomes using kallisto. 
# Kallisto uses pseudo-alignment to the reference genome (independently from what STAR has mapped). 
# It will generate tables of expression levels for different transcript isoforms. 
# We could also use STAR modeCount to simply count the mapped instances, but this will not give us isoform overview, or statistical assessment of the results. 
#
# NOTE: This is the peraratory script that will make KALLISTO index the reference genome... 
# 
#


#SBATCH --mail-type=end,fail
#SBATCH --mail-user=noah.kleinschmidt@students.unibe.ch
#SBATCH --job-name="Index KALLISTO"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=10
#SBATCH --time=08:00:00
#SBATCH --mem=100G

# load the module
module add UHTS/Analysis/kallisto/0.46.0;

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
READS_DIR=$USER_DIR/reads

KALLISTO_DIR=$USER_DIR/KALLISTO

cd $KALLISTO_DIR/ref
# get refernce ref_transcriptome

# we must not use the genome.fa but get the transcript.fa specifically from NCBI
# hence we download the file and then index it

# NOTE: WE updated the reference genome, using the one directly from gencode. 
#       Just to be sure, we now also download the corresponding transcript.fa file...
# this was the old one...
# wget https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_38/gencode.v38.transcripts.fa.gz
wget https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_39/gencode.v39.transcripts.fa.gz


ref_transcriptome=*transcripts.fa.gz

# index genome 
# --make-unique to ensure all targets have unique names 
# kmer size is default 31 
# --index = the output filename
kallisto index --index ${ref_transcriptome}_kallisto.idx  $ref_transcriptome

