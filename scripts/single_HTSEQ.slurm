#!/bin/bash

# This is step 4.3: 
# We now actually do quantify our transcriptomes, but with HTSeq.
# We have to do this, since Kallisto is unable to work with un-annotated transcripts. 
# Hence, we perform HTSeq quantification, followed by DESeq analysis to check for un-annotated transcripts as well...  
#
# NOTE: This is the actual slurm script.
# 
#
# Expected arguments are: 
# ---------------------------------------------------------------------------------------------
#
#   $1    The name of the biological sample within the STAR directory
#
# ---------------------------------------------------------------------------------------------



#SBATCH --mail-type=end,fail
#SBATCH --mail-user=noah.kleinschmidt@students.unibe.ch
#SBATCH --job-name="HTSeq"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --time=06:00:00
#SBATCH --mem-per-cpu=5G

# load the module
module add UHTS/Analysis/HTSeq/0.9.1

REF_DIR="/data/courses/rnaseq/lncRNAs/Project1/references"
USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
STAR_DIR=$USER_DIR/STAR_ungrouped
STAR_REF_DIR=$USER_DIR/STAR_ref

STRINGTIE_DIR=$USER_DIR/STRINGTIE

HTSEQ_DIR=$USER_DIR/HTSEQ

# get working cell_line
sample=$1

# get input BAM File
bam_file=$STAR_DIR/$sample/*.bam

# define output directory (and make it if necessary, 
# though it should already be present)
# --counts_output=$OUTPUT_FILE
OUTPUT_DIR=$HTSEQ_DIR/$sample
OUTPUT_FILE=$OUTPUT_DIR/${sample}_quant.tsv

if [[ ! -d $OUTPUT_DIR ]]; then 
    mkdir $OUTPUT_DIR
fi 

# get refernce gtf file 
# we use the gff3 file in the project reference folder (since this was also used by KALLISTO, so we get the same IDs, hopefully...)
# REF_GTF=$STAR_REF_DIR/gtf/*.gff
#
# BIG NOTE: We need to use OUR OWN GTF Annotatino file, since this is the only one that retains information about non-annotated transcripts that were found in our bam files...
#           This GTF annotation file would still contain all the information from the gencode that we need (such as ID) (at least for all the annotated hits)...

# REF_GTF=$REF_DIR/gencode.v38.annotation.gff3
REF_GENOME_GTF="$STRINGTIE_DIR/merged/all_gtf_merged_corrected.gtf"


# Set the ID feature within the GTF File
# Our GFF File contains no gene_ID feature, instead we have a Parent entry, we can use as gene ID
# UPDATE: since switching to the project-reference folder gff3 file, there should be ID in there...
# --idattr=$ID
# ID="Parent"
ID="transcript_id"

# We also include the generic Gene name (gene) as additional attribute as well as the gbkey feature (seems to be kinda a "kind of transcript" sort-of-thing)
# --additional-attr="gene"
# --additional-attr="gbkey"

# Setup ordered setting from BAM file
# we used SortedByCoordinate in STAR, hence the ordering should be according to position (pos)
# --order=$ORDERED_AS
ORDERED_AS="pos"

# feature type
# may be either exon or transcript
# --type=$TYPE
TYPE="transcript"


# call htseq-count                                                                
htseq-count --format=bam --order=$ORDERED_AS --type=$TYPE --idattr=$ID --additional-attr="gene_name" --additional-attr="gene_id" $bam_file "$REF_GENOME_GTF" > $OUTPUT_FILE
