#!/bin/bash

# This is step 6.5.2: 
# We check for TSS based on a TSS dataset which we process using our own custom python framework...
#
# This script combines all _polya.csv output files into a single one and removes the individual headers (leaving the total file without a header!)
#

USER_DIR="/data/courses/rnaseq/lncRNAs/Project1/users/noah"
cd $USER_DIR

cd $USER_DIR/pytss_results

files=$(ls *polya.csv)

cat $files | grep -v "id" >> polya.csv