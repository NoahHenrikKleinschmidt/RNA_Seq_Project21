# RNA-Seq Project 2021

This project set out to identify new long-non-coding RNAs (lncRNAs) from human lung cancer cell lines. 

Of special interest of the underlying project is to investigate the effect of depletion of lactate dehydrogenase B (LDHB), an enzyme involved in lactate metabolism. This is of special interest as lactate has been found to be involved in various metabolic processes that favour tumorigenisis and mestastogenesis. (NEEDS SOME MORE SOURCES HERE...). LDHB in cells was depleated by Knockdown using RNAi as well as genetic Knockout. 

The goal of the bioinformatic part of this project (i.e. our part) is to analyse the transcriptome data from next-gen sequencing done on said cell lines and identify differentially expressed reads in non-coding regions that could potentially be linked to changing LDHB activity/presence. 

> NOTE: This is an outdatad version of the workflow!
> Please, refer to the workflow outlined in the `complete_workflow` text file or the report. 

## Data of Interest
---
Of primary interest are reads not mapped to any coding genomic regions. Furthermore, reads that are either significantly increased or decreased in KD/KO samples compared to CTRL samples are of particular interest. 


# Workflow

The data processing workflow and application of scripts written is outlined below. 

## Data Processing and STAR Mapping
---
### 1. Collecting and sorting data
In order to collect the data from the central server directory (fastq subfolder in Project directory) the script `sort_data.slurm` was written and executed. It creates a new folder `reads` within the user directory within the project (referred to as home_user henceforth) and makes a separate subfolder for each cell line sample, storing the `fastq.gz` sequencing data in the subfolder `raw` alongside a copy of the reference genome in the `ref` subfolder.

### 2. Quality Control 
To assess the quality of the sequencing data `fastqc` and `multiqc` were used. To that end the script `single_QC.slurm` and wrapper `perform_QC.sh` were written and executed. They perform `fastqc` quality control and summarise results using `multiqc`, storing the results in a new subfolder `MultiQC` within each cell line sample folder. 

##### 2.5 Download MultiQC Reports
In order to facilitate extracting the multiqc reports from all cell line sample folders the script `get_multiqc.sh` was written, which will copy all multiqc.html reports into a single folder `MultiQC` within home_user. 

> ###  Grouped_Samples
> NOTE: As the different samples belong to biological replicates, a separate GIT branch was developed called "grouped_samples" That modified the hereafter mentioned scripts to merge the biological sample data files into single-units that were analysed together. 
> The descriptions below are mostly from the pre-grouping branch and notes are added describing alterations in the scripts...
> **CONCEPT:** Instead of iterating over each cell_line (HD1, ...) in READS_DIR a new file was written in home_user `replicates.csv` that connects the biological replicates to their corresponding samples. So instead of iterating over READS_DIR folders, the iteration was done over the lines in `replicates.csv` and all data-fastq files from the therein specified HD-lines were assembled together, for a combined sample in STAR and STRINGTIE and KALLISTO folders...

### 3. Perparation for STAR Mapping
In order to align the sequencing reads to the reference genome the `STAR` software was used. To that end the script `perp_for_STAR.slurm` was written and executed, which will generate a new folder `STAR_ref` within home_user, containing subfolders `genome` (again a copy of the reference genome) and `gtf` which contains the current genome annotation from *NCBI* corresponding to the reference genome. 

As `STAR` requires an indexed genome to map reads to, this script further indexed the reference genome using the annotation file, storing the indexed genome in the subfolder `genome`. 

> Note that this step is required only once and does not need to be repeated!

### 4. STAR Mapping
To map reads to the reference genome using `STAR` the script `single_STAR.slurm` and wrapper `perform_STAR.sh` were written and executed. They perform STAR mapping of all replicate paired reads of each cell line sample and store results within a dedicated cell line subfolder within a new directory `STAR` in home_user.

> EDIT: `single_STAR.slurm` was slightly modified to also include the  option  `--outSAMstrandField intronMotif ` to allow for reads accross splice junctions, which we would also be interested in.
> NOTE: results from the pre-grouping analysis are in the folder `STAR_ungrouped`

##### 4.5 Sorting STAR results
As STAR placed all results directly within the STAR folder (instead of the designated subfolder, why??) the script `sort_after_STAR.sh` was written and executed. It performs the sorting into the designated subfolders in the `STAR` directory. 
> EDIT: The error was found in `single_STAR.slurm` where 
> ```
>    # --outFileNamePrefix $OUTPUT_DIR 
>    OUTPUT_DIR=$STAR_DIR/$cell_line/
>    ```
> was missing the final `/`. Hence, `sort_after_STAR.sh` should not be required anymore.

## Transcriptome Assembly and Quantification
---

### 5. Transcriptome Assembly using STRINGTIE
To now assemble a transcriptome `gtf` annotation from the generated .bam  files from STAR we use `STRINGTIE`. To that end the script `single_STRINGTIE.slurm` and wrapper `perform_STRINGTIE.sh` were written and executed.

##### Prep. for STRINGTIE
Because the default version of stringtie on the cluster `1.3.3b` kept raising errors with the genome annotation `gff` file, it was necessary to manually install the latest version of stringtie in home_user. To that end the script `prep_for_STRINGTIE.slurm` was written and executed. It downloads and extracts a compiled binary of stringtie in a directory in home_user of the same name. `single_STRINGTIE.slurm` was altered accordingly to execute this binary instead of the pre-installed stringtie version.
Hence, it is required to run `prep_for_STRINGTIE.slurm` prior to  `perform_STRINGTIE.sh` so that a stringtie installation will be present in `home_user/stringtie/stringtie`.

> EDIT: After a first run of single_STAR.slurm no mappings using stringtie were possible due to "invalid genome data" from the reference file. It is so far not clear how this error came to pass (ask next time...). Nevertheless, research on the subject suggested that it might be a good idea to re-run single_STAR.slurm adding the parameter `--outSAMstrandField intronMotif ` to allow for reads accross splice junctions, which we would also be interested in. 
> Therefore, the script was modified with that and re-run. Original results are retained in the folder `STAR_no_intronMotif`.  

##### Quality Control
To assess the quality of the GTF assembly the script `get_STRINGTIE_stats.slurm` alongside with python module and script `gtf.py` and `get_gtf_quality.py` were written and executed. They produce a `csv` file within a given directory (i.e. STRINGTIE in this case) named `stats.csv` providing information about the number of entries in the assembly, the number of distinct transcripts found, mapped versus unmapped reads, and single-exon entries. 

### 6. Transcriptome Quantification using KALLISTO
To quantify the expression of the found transcripts `KALLISTO` software was used. To that end the scripts `single_KALLISTO.slurm` and `perform_KALLISTO.sh` as well as `prep_for_KALLISTO.sh` and `index_KALLISTO.slurm` were written and executed. 

##### Prep. for KALLISTO
As KALLISTO requires an indexed version of the transcriptome (as opposed to the actual whole genome used by STAR or STRINGTIE) it was necessary to generate a KALLISTO indexed version of the latest human transcriptome (from NCBI). To that end `prep_for_KALLISTO.sh` calls on `index_KALLISTO.slurm` to download the latest transcriptome and perform indexing, generating a new folder "KALLISTO/ref" within home_user. 

##### Sample Quantification
To then qunatify the sample transcripts as well as record pseudo-alignment done by KALLISTO, the scripts `single_KALLISTO.slurm` and `perform_KALLISTO.sh` were used. They generate a new subfolder for each cell line within the KALLISTO folder in home_user, storing an abundance table (as h5 and tsv) as well as a bam file for the pseudo-alignment. 

### 7. Differential Expression Analysis using Sleuth
To analyse the KALLISTO quantified data, `Sleuth` software was used. To that end, the R-Script `analysis.R` was written and executed, which computes both LRT and WT tests for each cell line separately and exports tabulary results on gene and transcript level (LRT) as well as transcript level results for WT (merged with corresponding LRT). Furthermore, the sleuth objects are also exported, for export an re-use on local machines (to facilitate using sleuth_live if desired, since this is not possible on the cluster).

##### Sleuth Data Visualisation
To aide with data visualisation, the python module `py_sleuth.py` was written which defines a class `SleuthResults` that loads tabulary sleuth results files and comes with a `volcano` plot function as well as a number of features for subsetting the data based on pvalue or fold-change information (or highlighting specific genes of interest). Additionally, it log-transforms certain values within the sleuth data. 

Both `sleuth` and `py_sleuth` were used within `Jupyter` Notebooks during data analysis. 

> **NOTE:** 
> KALLISTO only works with already known (= annotated) transcripts, hence Sleuth results are only including known transcripts. To address this issue, quantification was repeated using `HT-Seq` and `DeSeq2` to assess any as yet unknown / unannotated transcripts.

### 8. Novel Transcripts
To assess how many transcripts were not annotated to any known gene, the STRINGTIE meta-assembly was queried for any transcript entries that were not associated with a gene ID. To that end the script `subset_novel_transcripts.sh` was written and executed, which returns a csv file counting the total number of found transcripts alongside the number of annotated ones (leaving their difference as the novel/unannotated ones).

#### 8.1 Quantifing Novel Transcripts
To include Novel Transcripts in the quantification (since they were omitted by KALLISTO), a repeated quantification step was done using `HTSeq-count`. To that end the script and wrapper `single_HTSEQ.slurm` and `perform_HTSEQ.sh` were written and executed. They store HTseq-count output tables into a directory HTSEQ within USER_DIR, for later analysis using DESeq2.

### 9. Sequence-End Quality assessment
In order to assess the 5' and 3' End annotation of the sequence alignment, `ESAT` software was used. ESAT uses a very similar approach to FANTOM CAGEr but is written in Java and uses a command-line tool instead of a scriptable R package setup. 
> UPDATE: ESAT turned out to be kinda non-functional, so we are trying to use CAGEr after all...