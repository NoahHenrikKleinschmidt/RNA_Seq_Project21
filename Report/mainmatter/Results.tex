%\begin{multicols}{3}
\section{Results}


\subsection{Read Quality, Mapping, and Assembly}
To assess read quality, \texttt{MultiQC} reports were examined. Pre-base-quality of sequence reads was very stable throughout sequence length and over datasets, consistently being in the "green zone" (Fig. \ref{Fig:stats}E). Likewise no datasets failed due to overrepresented sequences. However, some datasets showed elevated levels of duplicated sequences, while no Adapter contamination was detectable. Taken together the quality overview of the data suggested applicability for further downstream processing and analysis. 
Reads were mapped to the genome using \texttt{STAR}, which resulted in an average 41'000'000 total processed reads, with an average percent of unique mapped reads of 82\%. Summary overview of mapping quality is given in Figure \ref{Fig:stats}D. 

Alignment was done both on each replicate individually as well as in a pooled manner per biological sample. For further downstream analysis of individually generated BAM files were used. However, this pooling step was included to provide an overview over the biological replicates already at assembly state. 

Transcriptome assemblies from the pooled alignments included on average around one million entries, including transcripts, exons and alike, on chromosomal loci. On average 100'000 distinctly labeled transcripts were assembled per biological sample, mapping to on average 23'000 distinct genes (Fig. \ref{Fig:stats}C). Between 12'000 and 15'000 transcripts did not receive an ENSEMBL identifier, marking them as potential novel transcripts (Fig. \ref{Fig:stats}A). Most of the assembled transcripts, novel and annotated, consisted of two or more exons, with a single-exon rate of around 10\% (Fig. \ref{Fig:stats}B). A merged annotation was generated from the aforementioned assemblies of each biological sample, containing around two million entries and 260'000 distinctly labelled transcripts, which mapped to 59'000 distinct genes (Fig. \ref{Fig:stats}B & C, right-most bar). Cross-referencing with a GTF assembly generated from GTF files based on each replicate individually, showed near identical number of entries, suggesting the two-step merging of the total assembly did not markedly interfere with the quality of that overall assembly. 



\begin{figure*}[t!]
	\includegraphics[width = \linewidth]{"../Analysis/Plots/volcano_plots_new"}
	\caption{
		Differentially expressed transcripts. 
		Gray markers indicate individual transcripts. Red markers highlight transcripts classified as differentially expressed based on criteria $q < 0.01$ ($q = $q-value, or adjusted p-value) and Fold-Change $> 1$ (in either direction). Blue markers highlight transcripts that were detected by both methods. Note that this does not indicate that both methods agree on their classification for a given transcript as differentially expressed, or not. 
		Shown are the results from the comparison of Knockdown (KD) \textit{versus} Control (CTR) of each cell line separately for each analysis method (\texttt{sleuth} and \texttt{DESeq2}).
		(A-C) Results from \texttt{sleuth} analysis using both LRT and WT. Plotted are $\beta$-value, which is \texttt{sleuth}'s analogue of $log_2$(Fold Change), against $-log_{10}$(q-value) from \texttt{sleuth}'s WT, while coloration is done based on the more reliable q-value from \texttt{sleuth}'s LRT.
		(D-F) Results from \texttt{DESeq2} analysis using WT. Plotted are $log_2$(Fold-Change) against against $-log_{10}$(adjusted p-value).
		(G) Overlaps between analysis results. Shown are the number of transcript entries shared between two datasets. 
		(H) Overlaps between transcript subsets declared as differentially expressed by both \texttt{sleuth} and \texttt{DESeq2}. 
		The amount of overlap, i.e. the number of transcripts shared between two sets, is indicated by color-gradient where a deeper hue indicates a greater number of shared transcripts. 
	}
	\label{Fig:volcano}
\end{figure*}


\subsection{Quantification}
\label{Sec:Quant}

To quantify the obtained reads, two separate approaches were followed, applying \texttt{KALLISTO} for subsequently analysis by \texttt{sleuth} (approach 1) and \texttt{HTSeq} for subsequent analysis by \texttt{DESeq2} (approach 2). It is important to note at this point that this dual strategy was followed for two reasons. Firstly, to have a second method of detection to enhance confidence in any hits marked as "differentially expressed" later on. The chief motivation at this point was the higher fidelity of \texttt{sleuth}'s LRT compared to WT of other methods with regard to false discovery rate. 

Secondly, it is noteworthy that in order to identify differentially expressed novel transcripts, \texttt{KALLISTO}-based counting is not suitable. This is because \texttt{KALLISTO} is not designed to work with novel transcripts \textit{per se} as it does not rely directly on alignments but has its own streamlined "pseudo-alignment" method that excludes non-annotated entries. \texttt{HTSeq} on the other hand is not limited in this regard and performs a simpler counting method which retains novel transcripts for subsequently analysis by \texttt{DESeq2}. 

\texttt{HTSeq} counting was performed non-strandedly as several features within the \texttt{GTF} assembly did not include strand information, forcing the counting to abandon strandedness. Hence, the total TPM count exceeded the expected one million by a reliable factor of two. This was not the case for \texttt{KALLISTO} quantification, which hit the one million TPM mark quite precisely in all replicates. Furthermore, counting tables from \texttt{KALLISTO} showed a substantially larger number of non-zero counted transcripts as compared to \texttt{HTSeq}. 






\begin{figure*}
	\includegraphics[width = \linewidth]{"../Analysis/Plots/novels_and_lncrna"}
	\caption{
		lncRNAs and novel transcripts. 
		(A-C) Shown are the results from the comparison of Knockdown (KD) \textit{versus} Control (CTR) of each cell line separately from \texttt{sleuth} analysis data.
		 Plotted are $\beta$-value, which is \texttt{sleuth}'s analogue of $log_2$(Fold Change), against $-log_{10}$(q-value) from \texttt{sleuth}'s WT, while coloration is done based on the more reliable q-value from \texttt{sleuth}'s LRT. 
		(D-F) Novel transcripts within the \texttt{DESeq2} analysis dataset. Plotted are $log_2$(Fold-Change) against against $-log_{10}$(adjusted p-value).
		Please note that the legend annotations are strictly sequential. Hence, lower entries represent subsets of the ones above.
	}
	\label{Fig:lncRNA_novels1}
\end{figure*}

\begin{figure*}
	\includegraphics[width = \linewidth]{"../Figures (other)/novels_figure_logscale"}
	\caption{
		Novel transcripts.
		(A) Coding potential of novel transcripts, predicted by \texttt{CPAT}. Shown are the number of transcripts whose best scoring ORF received a coding probability $p$ of over 90\%, between 90\% and 10\%, and below 10\%. 
		(B-C) Cumulative alignment scores for TSS (B) and polyAS (C) within novel transcripts. Plotted are the mean alignment score within the effective feature range $\widebar{\alpha_{eff}}$ (0-600 for TSS, $(l-600)$-$l$ for polyA, where $l$ is the total length of the transcript), against the total mean alignment score $\bar{\alpha}$ (from 0-$l$). Highlighted in red are transcripts whose $\widebar{\alpha_{eff}} > s \cdot \bar{\alpha}$, and in blue $\widebar{\alpha_{eff}} < s^{-1} \cdot \bar{\alpha}$, where $s = 1.5$ is a threshold for the $\widebar{\alpha_{eff}}$-fold change. 
		(D-E) Random sample of alignment scores for both TSS (D) and polyA (E) over transcript sequences. A black line denotes the start of the actual transcript, while a red/blue line denotes the end of the actual transcript (everything outside is the 500bp buffer). Cumulative alignment scores are indicated by color-gradient where more intense hue indicates higher alignment values clustered around a given position. 
	}
	\label{Fig:novels}
\end{figure*}



%\begin{table}[]
%\begin{tabular}{@{}lll@{}}
%\toprule
%Cell Line & Method & DE \\ \midrule
%A549 & sleuth & 1111 \\ 
%     & DESeq2 & 258  \\
%H358 & sleuth & 107  \\
%     & DESeq2 & 53   \\
%H838 & sleuth & 42   \\
%     & DESeq2 & 27   \\ \bottomrule
%\end{tabular}%
%\caption{
%	Number of transcripts found as differentially expressed in the KD - CTR comparison for each cell line and method. 
%}
%\label{Tab:DE}
%\end{table}


\subsection{Differential Expression}
Analysis of differential expression (DE) was performed using \texttt{sleuth} and \texttt{DESeq2} separately. Results from \texttt{sleuth} are considered the main results due to the higher fidelity of \texttt{sleuth}'s Log-Likelihood Test (LRT) compared to the commonly applied Wald Test (WT) of \texttt{DESeq2} and others. Hence, \texttt{DESeq2} analysis was performed as an auxiliary support, as well as to include DE-estimates for novel transcripts (as outlined above, see \ref{Sec:Quant} Quantification). Since all three cell lines included control (CTR) and knockdown (KD) experiments, analysis was focussed on this comparison, and results are presented below. Analysis of A549 knockout (KO) and non-targeting control (NTC) was limited to producing explorable volcano plots, which can be found in the project repository (see \ref{Sec:Availability} Availability). As threshold criterium to declare a given transcript differentially expressed, q-value $< 0.01$ (for \texttt{sleuth}, adjusted p-value for \texttt{DESeq2}) was chosen together with $abs(FC) > 1$, where $FC$ was either the $\beta$-value of \texttt{sleuth}'s WT or the $log_2$(Fold-Change) reported by \texttt{DESeq2}. The analysis, alongside with intermediary results and transcript subsets can be found in the \texttt{jupyter} notebook \texttt{DE\_results.ipynb}.

As expected, LDHB was found differentially expressed (enriched in CTR) across all cell lines in all \texttt{sleuth} datasets by at least two distinct transcripts (ENST00000350669.5, and ENST00000673047.2). However, these same transcripts were conspicuously undetected by \texttt{DESeq2}.

As for DE transcripts from the \texttt{sleuth} analysis, 1111 transcripts met the above mentioned DE criteria in the KD-CTR comparison in A549. \texttt{sleuth} further detected 107 and 42 DE transcripts in H358 and H838, respectively. \texttt{DESeq2} produced on the same cell lines 258, 53, and 27 DE transcripts, respectively.

Figure \ref{Fig:volcano} shows volcano plots of transcripts in the KD - CTR comparison in all three cell lines for both methods. It is noticeable that only one single transcript in A549 was declared DE by both \texttt{sleuth} and \texttt{DESeq2} (highlighted in green in Fig. \ref{Fig:volcano}A and D). This one transcript was the lncRNA ENST00000612722.1 (FLJ16779). However, this particular transcript was only declared DE in A549. The other two cell lines did not produce any transcripts dually declared DE transcripts. Comparing across cell lines, \texttt{sleuth} and \texttt{DESeq2} agreed on DE declaration for 74 transcripts in total. 

\texttt{sleuth} generally detected a substantially larger number of transcripts overall and produced consequently a greater number of "differentially expressed" hits (as readily visible from the vastly different number of data points on the volcano plots in Figure \ref{Fig:volcano}A-F). On average \texttt{sleuth} results on transcript-level included around 80'000 entries, while \texttt{DESeq2} results included some 13'000 transcripts. 

The amount of overlap between the analysed datasets was determined and overlaps are shown as chord charts in Figure \ref{Fig:volcano}G and H. Datasets from the same method showed substantial overlap, while the datasets differed noticeably between methods. However, considering the markedly differing size of the datasets it may be understandable that numerically only limited overlap was observable. Comparing the overlapping hits in the volcano plots (highlighted blue) reveals that most of the transcripts found by \texttt{DESeq2} are also part of the \texttt{sleuth} dataset, with the exception of any novel transcripts. 

\subsection{lncRNAs}
To identify lncRNAs within the dataset, \texttt{sleuth} data was directly queried for "lncRNA" as the dataset contains an extensive \texttt{target\_id} column which contains several versions of transcript and gene identifiers, as well as classifications. By this approach 5549 entries for known lncRNAs were identified, 60 of which were declared DE by \texttt{sleuth} across all cell lines. These were subsequently queried against all datasets which revealed four lncRNA transcripts declared DE across all cell lines by both methods. These were namely: ENST00000569981.2 (AC009133.12), ENST00000562897.1 (RP11-416I2.1), ENST00000378953.8 (IRF1 antisense RNA 1), and ENST00000589796.2 (AC138150.2). When restricted to \texttt{sleuth} datasets only, 36 transcripts were declared DE across cell lines, among these well known lncRNAs such as DANCR. As mentioned above one lncRNA (ENST00000612722.1, FLJ16779) was the only transcript found DE by both \texttt{sleuth} and \texttt{DESeq2} in the same cell line (A549).

\subsection{Novel Transcripts}
Transcripts were declared potentially "novel" if they did not receive a reference gene id in the merged \texttt{GTF} assembly file. 25007 transcripts matched that criterium, which were assembled into 11733 distinct loci (labelled as "genes" within the assembly). To assess the coding potential of these novel transcripts, sequences were extracted for these transcripts and analysed for potential open reading frames (ORF) using \texttt{CPAT}. 21'193 transcripts showed at least one potential ORF. For 7099 transcripts the coding probability $p$ of the "best" ORF was $p > 0.9$ (Fig. \ref{Fig:novels}A). Assessment of coding probability of the remaining transcripts suggested some 4874 had a coding probability below 10\% and 201 did not show any open reading frame, suggesting these transcripts may be potential novel lncRNAs. 

To further elucidate the end annotations of the novel transcripts, known sequence motifs of TSS and polyAS were aligned to transcript sequences using the developed \texttt{seqmap} module. This allowed easy inspection of alignment peaks for both TSS and polyA within transcript sequences. Transcripts were declared a "good" TSS fit if the mean of alignment scores within the starting range $\bar{\alpha_s}$ (up to 100 basepairs into the actual transcript, including a total range from 0 - 600, given the 500 basepair symmetric end-buffer) exceeded 1.5 times the overall mean alignment score $\bar{\alpha}$ of the sequence. The same procedure was applied for polyAS. 
Figure \ref{Fig:novels}B and C show the mean alignment score within the effective feature range (start or end region for TSS and polyAS, respectively) plotted against their respective $\bar{\alpha}$. Based on the outlined approach 6286 transcripts were declared well annotated for TSS, and 1546 for polyAS (highlighted in red). 

However, it should be noted that due to the small number of reference sequences used for polyA that number should be viewed cautiously. Figure \ref{Fig:novels}D and E show the mapped alignment scores of a random sample of transcripts. Noticeably TSS (Fig. \ref{Fig:novels}D) cluster well around the transcript start for multiple transcripts such as MSTRG.2775.3 or MSTRG.33074.3 (first), which would likely classify them as well annotated. For other transcripts such as the MSTRG.1567.4 multiple transcription start sites are readily visible that scatter throughout the transcript length. The same observations cannot be made for polyAS given the small reference data size and shortness of sequences which make them more likely to align at random positions within the sequence (Fig. \ref{Fig:novels}E). 


%\end{multicols}