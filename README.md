# RNA-Seq Project 2021
### Assessing the impact of LDHB-depletion on Differential Gene Expression in Human Lung Carcinoma cell lines

---

This project set out to identify new long-non-coding RNAs (lncRNAs) from human lung cancer cell lines. 

Of special interest of the underlying project is to investigate the effect of depletion of lactate dehydrogenase B (LDHB), an enzyme involved in lactate metabolism. This is of special interest as lactate has been found to be involved in various metabolic processes that favour tumorigenisis and mestastogenesis. LDHB in cells was depleated by Knockdown using RNAi as well as genetic Knockout. 

The goal of the bioinformatic part of this project (i.e. our part) is to analyse the transcriptome data from next-gen sequencing done on said cell lines and identify differentially expressed genes with a focus on long non-coding RNAs.

---

