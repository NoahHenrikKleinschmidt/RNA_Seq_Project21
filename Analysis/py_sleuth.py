"""
This module is designed to help with the analysis of sleuth analysis data
It includes functions for data visualisation and filtering
"""

import pandas as pd 
import scipy as sc
import numpy as np
import seaborn as sns 
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D

import os 
import glob 
from bokeh.layouts import gridplot
from bokeh.models import ColumnDataSource, Title
from bokeh.plotting import figure, show, output_file, save
import bokeh 
bokeh.io.output_notebook()


class DEResults:
    """
    This generic superclass is designed to handle text datafiles from Sleuth or DESeq2
    """
    def __init__(self, directory, name=None):
        self.directory = directory
        self.name = name if name is not None else os.path.basename(directory)
        self._metadata = None
        self._wt_transcript = {} # the default dictionary which will store wald_test results (both sleuth and DEseq2 use wald tests...)
        self._default_x = None # default columns to use for volcano plot...
        self._default_y = None
        self._default_pval = None
        self._default_x_label = None
        self._default_y_label = None
        
    def setup(self, filename, subset=None, **kwargs):
        """
        Loads a sleuth metadata file (in csv format).
        Using the subset argument, a specific subset can be selected for the metadata (i.e. only a single cell line...)
        the subset argument will be directly passed as query to the pd dataframe.
        """
        self._metadata = pd.read_csv(filename)
        if subset is not None: 
            self._metadata = self._metadata.query(subset)
        
        # # load data after setup...
        # try: 
        #     # self.load_expression_matrix(**kwargs)
        #     # self.load_lrt_results(**kwargs)
        #     self.load_wt_results(**kwargs)
        # except:
        #     print("Some data could not be loaded!\nThis might be due to non-default filenames. Try loading data manually and specify the filenames you used.")
    
    def export(self, loc=None):
        """
        Saves the WT data tables to files in location loc (default = source directory)...
        """
        save_loc = loc if loc is not None else self.directory
        for test, df in self._wt_transcript.items():
            filename = os.path.join(save_loc, f"DEResults_{self.name}_wt_{test}_transcript.csv")
            df.to_csv(filename, index = False)


    def volcano(self, test, fc_threshold = 1, qval_threshold = 0.05, mode = "static", **kwargs):
        """
        Generates either a static or interactive volcano plot for a given WT test result.
        Note, that the interactive mode only exports an HTML file of the chart.
        The static mode allows additional highlighting of individual gene-transcripts as well as more coloring options.
        """
        if mode == "static":
            result = self._static_volcano(test, fc_threshold, qval_threshold, **kwargs)
            return result
        elif mode == "interactive":
            p = self._interactive_volcano(test, fc_threshold, qval_threshold, **kwargs)
            return p 

    def wt_tests(self):
        """
        Returns a list of available WT Tests
        """
        return list(self._wt_transcript.keys())

    def get_wt(self, wt_test):
        """
        Returns a wt_test dataframe
        """
        return self._wt_transcript[wt_test]

    def wt_log_transform(self, col, src = None, **kwargs ):
        """
        Transforms a given column to -log10 in either a specific dataframe
        (src) or in all wald test stored in the object (default).
        """

        log_funcs = {
            "log10" : np.log10,
            "-log10" : np.log10,
            "log2" : np.log2,
            "-log2" : np.log2,
        }

        log_func = kwargs.pop("log_func", "log10")
        negative = True if "-" in log_func else False
        new_name = kwargs.pop("name", f"{log_func}{col}")
        if "-" in new_name: new_name = new_name.replace("-", "neg_")

        logfunc = log_funcs[log_func]

        if src is None: 
            for i in self._wt_transcript.values():
                self._apply_logfunc(i, col, negative, new_name, logfunc)
        else: 
            self._apply_logfunc(src, col, negative, new_name, logfunc)
            return src

    def _apply_logfunc(self, df, col, negative, new_name, logfunc):
        """
        Applies the logfunc to the a df 
        """
        df[new_name] = logfunc(df[col])
        if negative:
            df[new_name] = -df[new_name]

    def _static_volcano(self, wt_test, fc_threshold = 1, pval_threshold = 0.05, highlight_genes:list = None, **kwargs):
        """
        Generate a volcano plot of a given WT 
        test results, highlighting transcripts of a list of genes of interest...
        """
        ax = kwargs.pop("ax", None)
        df = kwargs.pop("data", self._wt_transcript[wt_test])
        xvals = kwargs.pop("xvals", self._default_x)
        yvals = kwargs.pop("yvals", self._default_y)
        pvals = kwargs.pop("pvals", self._default_pval)

        # base threshold for pvalues of base set
        base_threshold = kwargs.pop("base_threshold", 1)
        df = df.query(f"{pvals} < {base_threshold}")

        color_by_gene = kwargs.pop("color_by_gene", True)
        filter_gene_transcripts = kwargs.pop("filter_gene_transcripts", True)
        gene_column = kwargs.pop("gene_column", "ext_gene")
        show_signif_subset = kwargs.pop("show_signif", True)
        signif_subset_color = kwargs.pop("signif_color", ["blue", "crimson"])
        alpha = kwargs.pop("alpha_shift", 1)
        signif_query = kwargs.pop("signif_query", None)
        signif_divergent_scale = kwargs.pop("signif_divscale", False)
        highlight_color = kwargs.pop("highlight_color", "limegreen")

        crossref = kwargs.pop("crossref", True)
        crossref_marker = kwargs.pop("crossref_marker", "^")

        xlabel = kwargs.pop("xlabel", self._default_x_label)
        ylabel = kwargs.pop("xlabel", self._default_y_label)

        if ax is None: 
            fig, ax = plt.subplots()
        
        if signif_query is None:
            subset = df.query(f"abs({xvals}) > {fc_threshold}")
            subset = subset.query(f"{pvals} < {pval_threshold}")
        else:
            subset = df.query(signif_query)
        
        # plot baground dots
        sns.scatterplot(data=df, x = xvals, y = yvals, color = "lightgray", edgecolor = "lightgray", alpha = 0.1, ax = ax)
        
        # plot subset
        if show_signif_subset: 
            down_regulated = subset.query(f"{xvals} < 0")
            up_regulated = subset.query(f"{xvals} > 0")

            if signif_divergent_scale:
                scales = ["ch:start=.2,rot=-.3", "ch:s=-.2,r=.6"]
            else:
                scales = signif_subset_color

            for s, scale in zip([down_regulated, up_regulated], scales):

                if signif_subset_color in list(s.columns):
                    sns.scatterplot(data=s, x = xvals, y = yvals, hue = signif_subset_color, alpha = 0.4 * alpha, ax = ax)
                else:
                    sns.scatterplot(data=s, x = xvals, y = yvals, color = scale, edgecolor = scale,  alpha = 0.1 * alpha, palette = scale, ax = ax)
            
        sns.despine()

        # plot highlighted genes
        if highlight_genes is not None: 

            src = subset if filter_gene_transcripts else df
            tmp = src.query(f"{gene_column} in {highlight_genes}")

            if color_by_gene:
                sns.scatterplot(data=tmp, x = xvals, y = yvals, hue = gene_column, edgecolor = None, alpha = 0.7, ax = ax)
            else:
                sns.scatterplot(data=tmp, x = xvals, y = yvals, color = highlight_color, edgecolor = None, alpha = 0.7, ax = ax)
                ax.legend(title = "Highlighted", handles = [Line2D([0], [0], marker = "o", lw = 0, color = highlight_color, label = f"{i}") for i in tmp[gene_column]])


        if crossref: 
            overlap_cols = [i for i in df.columns if "overlap" in i]
            for overlap_col in overlap_cols:
                tmp = subset.query(f"{overlap_col} == True")
                
                if color_by_gene:
                    sns.scatterplot(data=tmp, x = xvals, y = yvals, marker = crossref_marker, hue = gene_column, edgecolor = None, alpha = 0.8, ax = ax)
                else:
                    sns.scatterplot(data=tmp, x = xvals, y = yvals, marker = crossref_marker, color = highlight_color, edgecolor = None, alpha = 0.8, ax = ax)
                    ax.legend(title = "Highlighted", handles = [Line2D([0], [0], marker = "o", lw = 0, color = highlight_color, label = f"{i}") for i in tmp[gene_column]])

        try: 
            ax.legend(bbox_to_anchor = (1,1))
            ax.set(xlabel = xlabel, ylabel = ylabel)
        except: 
            pass
        
        if highlight_genes is not None:
            return tmp, subset
        else:
            return subset

    def _interactive_volcano(self, wt_test, fc_threshold = 1, qval_threshold = 0.05, **kwargs):
        """
        Generate an interactive volcano plot of a given WT 
        test results, highlighting transcripts of a list of genes of interest...
        """

        xvals = kwargs.pop("xvals", self._default_x)
        yvals = kwargs.pop("yvals", self._default_y)
        pvals = kwargs.pop("pvals", self._default_pval)

        # source df
        source = kwargs.pop("data", self._wt_transcript[wt_test])

        # base threshold for pvalues of base set
        base_threshold = kwargs.pop("base_threshold", 1)
        source = source.query(f"{pvals} < {base_threshold}")


        gene_column = kwargs.pop("gene_column", "ext_gene")
        show_signif_subset = kwargs.pop("show_signif", True)
        signif_subset_color = kwargs.pop("signif_color", ["blue", "crimson"])
        alpha = kwargs.pop("alpha_shift", 0.2)
        size = kwargs.pop("size", 6)
        signif_query = kwargs.pop("signif_query", None)
        filename = kwargs.pop("output_file", f"./{self.name}_{wt_test}.html")
        
        xlabel = kwargs.pop("xlabel", self._default_x_label)
        ylabel = kwargs.pop("xlabel", self._default_y_label)

        # set labels
        source["transcript"] = source["target_id"].apply(lambda x: x.split("|")[0])
        
        if signif_query is None:
            subset = source.query(f"abs({xvals}) > {fc_threshold}")
            subset = subset.query(f"{pvals} < {qval_threshold}")
        else:
            subset = source.query(signif_query)

        output_file(filename)

        # setup tools
        tools = ["box_select", "box_zoom", "hover", "reset"]

        TOOLTIPS = [
            ("index", "$index"),
            ("name", f"@{gene_column}"),
            ("desc", "@transcript")
        ]

        # plot background
        p = figure(height=400, width=700, tools=tools, tooltips = TOOLTIPS)
        p.circle(x=xvals, y=yvals, size=size, hover_color="red", color = "lightgray", alpha = alpha, source=source)

        if show_signif_subset:
            # plot up and downregulated subsets...
            ups = subset.query(f"{xvals} > 0")
            p.circle(x=xvals, y=yvals, size=size, hover_color="red", color = signif_subset_color[1], alpha = alpha, source=ups)
            down = subset.query(f"{xvals} < 0")
            p.circle(x=xvals, y=yvals, size=size, hover_color="red", color = signif_subset_color[0], alpha = alpha, source=down)

        p.add_layout(Title(text=xlabel, align="center"), "below")
        p.add_layout(Title(text=ylabel, align="center"), "left")
        
        # save plot
        save(p)
        return p 

class DESeq2Results(DEResults):
    """
    Load DESeq2 generated datafiles (csv) stored in a common directory. 
    All files will be assumed Wald Test Results...
    """
    def __init__(self, directory, name=None):
        super().__init__(directory, name = name)
        self._default_x = "log2FoldChange"
        self._default_y = "neg_log10padj"
        self._default_pval = "padj"
        self._default_x_label = "log2 Fold Change"
        self._default_y_label = "-log10 P-Value"

    def load_wt_results(self, filename=None, name=None, **kwargs):
        """
        Loads the WT transcript level results (in csv format)
        """
        fname = filename if filename is not None else "*.csv"
        wt_results = glob.glob(os.path.join(self.directory, fname))
        if wt_results == []:
            raise(FileNotFoundError)
        for file in wt_results:
            
            n = name if name is not None else os.path.basename(file).replace(".csv", "") # default for new files
            n = n if "_wt_" not in n else n.split("_wt_")[1].replace("_transcript", "") # in case lazy loaded files were used...

            df = pd.read_csv(file)
            df = df.rename(columns = {"Unnamed: 0" : "transcript_id"})
            self._wt_transcript.update({ n : df })   

    def setup(self, filename, subset=None, lazy = True, export = True, **kwargs):
        """
        Loads a DESeq2/Sleuth metadata file (in csv format).
        Using the subset argument, a specific subset can be selected for the metadata (i.e. only a single cell line...)
        the subset argument will be directly passed as query to the pd dataframe.
        """
        super().setup(filename = filename, **kwargs)
        if lazy: 
            try: 
                self.load_wt_results(filename = "DEResults*", **kwargs)
                print(f"[{self.name}] Successfully loaded tests: ", self.wt_tests())
            except: 
                print(f"[{self.name}] Lazy load failed! Trying with regular...")
                self.load_wt_results(**kwargs)
                self.wt_log_transform("padj", log_func = "-log10")
                if export: 
                    export_loc = kwargs.pop("export_loc", None)
                    self.export(export_loc)
                    print(f"[{self.name}] Successfully exported!")
    
    def import_gene_labels(self, obj):
        """
        Imports gene labels and common names from a SleuthResults object
        """
        labels = obj._export_gene_aggregation_labels()
        for name, df in self._wt_transcript.items():
            self._wt_transcript[name] = df.merge(labels, on = "transcript_id")


class SleuthResults(DEResults):
    """
    Load sleuth generated datafiles (csv), stored in a common directory
    These must include: 
    - an expression matrix file (*_expression_matrix.csv)
    - transcript-level wt result files (*_wt_*_transcript.csv)
    - transcript-level lrt result files (*_transcript_level.csv)
    """
    def __init__(self, directory, name=None):
        super().__init__(directory, name = name)
        self._expression_matrix = None
        self._lrt_transcript = None
        self._lrt_gene = None
        self._deseq_objects = [] # link DESEq2Results objects for cross-referencing...
        self._default_x = "b"
        self._default_y = "neg_log10qval_wt"
        self._default_pval = "qval_wt"
        self._default_x_label = "Beta"
        self._default_y_label = "-log10 Q-Value"

    
    def setup(self, filename, subset=None, lazy = True, export = True, **kwargs):
        """
        Loads a sleuth metadata file (in csv format).
        Using the subset argument, a specific subset can be selected for the metadata (i.e. only a single cell line...)
        the subset argument will be directly passed as query to the pd dataframe.
        
        lazy loading tries to load pre computed results tables that already have log-transformed columns.
        This will speed up loading.
        """

        super().setup(filename=filename,subset=subset,**kwargs)
    
        # load data after setup...
        self.load_expression_matrix(**kwargs)
        self.load_lrt_results(**kwargs)
        
        if lazy: 
            try: 
                filepattern = "DEResults*"
                self.load_wt_results(filename = filepattern)
                print(f"[{self.name}] Successfully loaded tests:", self.wt_tests())
                return
            except: 
                print(f"[{self.name}] Lazy load failed!, Trying regular loading...")

        try:
            self.load_wt_results(**kwargs)
            self.wt_log_transform("pval_wt", log_func="-log10")
            self.wt_log_transform("qval_wt", log_func="-log10")
            self.aggregate_qvalues()
            if export:
                export_loc = kwargs.pop("export_loc", None)
                self.export(loc = export_loc)
                print(f"[{self.name}] Successfully exported!")
        except:
            print("Some data could not be loaded!\nThis might be due to non-default filenames. Try loading data manually and specify the filenames you used.")

    def load_wt_results(self, filename=None, name=None, **kwargs):
        """
        Loads the WT transcript level results (in csv format)
        """
        fname = filename if filename is not None else "*_wt_*_transcript.csv"
        wt_results = glob.glob(os.path.join(self.directory, fname))
        if wt_results == []: 
            raise(FileNotFoundError)
        for file in wt_results:
            
            n = name if name is not None else file.split("_wt_")[1].replace("_transcript.csv", "")
            df = pd.read_csv(file)
            self._wt_transcript.update({ n : df })    

    def load_expression_matrix(self, filename=None, group=True, by="condition", **kwargs):
        """
        Load an expression matrix file from sleuth, and groups the columns according to 
        assignments in the sleuth metadata file that was loaded with setup(). By default
        a column named 'condition' is assumed to be part of the metadata file. But any single column may be specified...
        """
        fname = filename if filename is not None else "*_expression_matrix.csv"
        expression_matrix = glob.glob(os.path.join(self.directory, fname))[0]
        expression_matrix = pd.read_csv(expression_matrix)
        expression_matrix = expression_matrix.rename(columns = {"Unnamed: 0" : "target_id"})        # group columns in expression matrix accodring to metadata...

        self._expression_matrix = expression_matrix

        if group:
            grouping_col = self._metadata[by]
            group_names = list(set(grouping_col))
            
            grouped_expression_matix = pd.DataFrame(expression_matrix["target_id"])
            for group in group_names:
                try:
                    subset = list(self._metadata.query(f"{by} == '{group}'")["sample"])
                    subset = expression_matrix[subset]

                    subset = subset.mean(axis = 1)
                    grouped_expression_matix[group] = subset
                except: pass

            self._expression_matrix = grouped_expression_matix

    def load_lrt_results(self, filename=None, **kwargs):
        """
        Loads the LRT transcript level results (in csv format)
        """
        fname = filename if filename is not None else "*_transcript_level.csv"
        lrt_results = glob.glob(os.path.join(self.directory, fname))[0]
        lrt_results = pd.read_csv(lrt_results)
        self._lrt_transcript = lrt_results

    
    def get_lrt(self, kind = "transcript"):
        """
        Returns a wt_test dataframe
        """
        df = self._lrt_transcript if kind == "transcript" else self._lrt_gene
        return df

    def aggregate_qvalues(self, agg_column = "ext_gene"):
        """
        Aggregates pvalues using Fisher's Method from to transform to gene level
        """
        
        genes = list(set(self._lrt_transcript[agg_column]))

        qvalue_aggregates = {
            "ext_gene" : [],
            "stat" : [],
            "agg_qval" : []
        }

        for gene in genes:
            tmp = self._lrt_transcript.query(f"ext_gene == '{gene}'")
            qvals = tmp["qval"]
            agg_qval = sc.stats.combine_pvalues(qvals)
            qvalue_aggregates["ext_gene"].append(gene)
            qvalue_aggregates["stat"].append(agg_qval[0])
            qvalue_aggregates["agg_qval"].append(agg_qval[1])

        qvalue_aggregates = pd.DataFrame(qvalue_aggregates)
        self._lrt_gene = qvalue_aggregates

    def link_deseq(self, obj):
        """
        Links a DESeq2Results object to crossreference common hits... 
        """
        self._deseq_objects.append(obj)

    def crossref_deseq(self, wt_test, pval_threshold = 0.05, fc_threshold = 1, **kwargs):
        """
        Cross references results from DESeq2Results with those from the SleuthResults object...
        Adds a logical column if a transcript was also found significant according to pval and/or fc threshold criteria, or an arbitrary signif_query.
        """

        signif_query = kwargs.pop("signif_query", None)

        if isinstance(wt_test, dict):
            wt_test_sleuth = list(wt_test.keys())[0]
            wt_test_deseq = list(wt_test.values())[0]
        else:
            wt_test_sleuth = wt_test
            wt_test_deseq = wt_test

        # add a matching transcript_id column to cross-reference with deseq (also store for re-use...)
        if "transcript_id" not in self._wt_transcript[wt_test_sleuth].columns:
            self._wt_transcript[wt_test_sleuth]["transcript_id"] = self._wt_transcript[wt_test_sleuth]["target_id"].apply(lambda x: x.split("|")[0])
        
        for deseq_obj in self._deseq_objects:
            
            # get corresponding test df
            try: 
                deseq_df = deseq_obj.get_wt(wt_test_deseq)
            except: 
                print(f"No test could be identified in {deseq_obj.name} for testname: {wt_test_deseq}!\nIf DESEQ and Sleuth use different names for the same tests, use a dictionary to specify which tests to match.")
            # get the significant / interesting subset
            if signif_query is not None: 
                deseq_df_signifs = deseq_df.query(signif_query)
            else:
                q = f"{deseq_obj._default_pval} < {pval_threshold} & abs({deseq_obj._default_x}) > {fc_threshold}"
                deseq_df_signifs = deseq_df.query(q)
            deseq_df_signifs = deseq_df_signifs["transcript_id"]

            # add logical column
            self._wt_transcript[wt_test_sleuth][f"overlap_{deseq_obj.name}"] = self._wt_transcript[wt_test_sleuth]["transcript_id"].isin(deseq_df_signifs)



    def _export_gene_aggregation_labels(self):
        """
        This function is designed to export a table of ENSG gene labels and gene names (ext_gene)
        to a DESeq2Results object that only contains transcript level annotation...
        This export will be done based on the target_id column (as it contains all required information...)
        """    
        df = pd.DataFrame(self._lrt_transcript[["target_id", "ext_gene"]])
        df["transcript_id"] = df["target_id"].apply(lambda x: x.split("|")[0])
        df["gene_id"] = df["target_id"].apply(lambda x: x.split("|")[1])
        return df

if __name__ == '__main__':
    a = SleuthResults(directory = "../data/testing/")
    a.setup("./index2.csv", subset = "cell_line == 'A549'")
    # print(a._metadata)
    # a.load_expression_matrix()
    a.wt_log_transform()
    a.aggregate_qvalues()
    print(a.get_wt("conditionKD"))
    print(a.get_lrt(kind = "gene"))