"""
This module defines the class Mapper that takes in a dataframe of transcript sequences of interest
and a corresponding dataframe of reference sequences to map onto. 
It makes use of Smith-Waterman local alignment and maps the alignment scores onto the overall sequence.
It ultimately saves a dataframe of scores mapped over the entire sequence, which can be visualised as a heatmap.
"""

import pandas as pd
import numpy as np
import subprocess 
import os 
import skbio


def read_fasta(filename, min_length = 10, max_length = 100, drop_duplicates = True, save = True):
    """
    Reads a Fasta File and reduces it to sequences of max_length => length >= min_length
    """
    tmpfile = "{}.tmp".format(filename)
    subprocess.run("echo 'id\tseq' > {}.table".format(tmpfile), shell = True)

    # get all headers and sequences
    subprocess.run("grep -Eo '[ATGC]{2,}' " + " {} > {}.seq".format(filename, tmpfile), shell =True)
    subprocess.run("grep -Eo '>(.+)' " + " {} > {}.name".format(filename, tmpfile), shell =True)

    # merge the two files...
    subprocess.run("paste {tmpfile}.name {tmpfile}.seq >> {tmpfile}.table".format(tmpfile = tmpfile), shell = True)
    
    tss_df = pd.read_csv("{}.table".format(tmpfile), sep = "\t")
    os.remove("{}.seq".format(tmpfile))
    os.remove("{}.name".format(tmpfile))
    os.remove("{}.table".format(tmpfile))

    # filter for sequence size to be in the specified range
    tss_df = tss_df[
                        [
                            max_length >= len(i) >= min_length for i in tss_df["seq"]
                        ]
                    ] 
                    
    tss_df = tss_df.dropna()
    if drop_duplicates:
        tss_df = tss_df.drop_duplicates(["seq"])
    if save:
        tss_df.to_csv("{}.csv".format(filename), sep = "\t", index = False, header = False)
    return tss_df

def buffer_gtf(filename, pre_buffer = 300, post_buffer = 200, save=True):
    """
    Extends the coordinates of start and stop for each feature (line) by the pre and post buffers...
    """
    names = ["chrom", "source", "feature", "start", "stop", "score", "strand", "frame", "attribute"]
    df = pd.read_table(filename, sep = "\t", names = names)
    df["start"] = df["start"] - pre_buffer
    df["stop"] = df["stop"] + post_buffer

    # reset indices to 1 in case buffering made them negative
    df["start"][df["start"] <= 0] = 1

    if save: 
        newfile = filename.replace(".gtf", "_buffered.gtf")
        df.to_csv(newfile, sep = "\t", index = False, header=False)
    return df


# The idea:
# We initialise for each seq (transcript) a new array for its positions
# and one for its potential tss sites. then we let each tss align and get the postiion alongside the identity 
# of the match. we will only look at the best match, hence we will only align each tss once to the seq
# and we will then gradually acumulate signals for tss...

class Mapper(object):
    """
    This class takes in a dataframe from reading a fasta file of sample transcripts and another dataframe of reference sequences to align to
    """
    def __init__(self, target_seqs, ref_seqs):
        self._ids = []
        self._lengths = []
        self._scores = []
        self._data = target_seqs
        self._ref_seqs = ref_seqs

    def get(self):
        """
        Returns the results dictionary as a pd dataframe
        """
                
        self._df = pd.DataFrame({"id" : [], "length" : [], "score" : []})

        return pd.DataFrame(self._df)

    def parse(self):
        """
        Parses the ref sequences given and aligns each, storing the aligned identities for each transcript in a dataframe
        """
        idx = 0
        for id, seq in zip(self._data["id"], self._data["seq"]):
            
            print("Reading: {}".format(id))
            length = len(seq)
            scores = np.zeros(length)
            seq = skbio.DNA(seq)

            for ref_t in self._ref_seqs["seq"]:
                
                ref_t = skbio.DNA(ref_t)
                alignment = skbio.alignment.local_pairwise_align_ssw(seq, ref_t)
                start, stop = alignment[2][0]
                score = alignment[1]
    
                # update the scores
                scores[slice(start, stop)] += score
            idx += 1
            self._ids.append(id)
            self._lengths.append(length)
            self._scores.append(scores)
