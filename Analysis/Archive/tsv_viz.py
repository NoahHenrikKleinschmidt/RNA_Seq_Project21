"""
This module is designed to assist with working with abundance.tsv file data and their analysis and visualisation
"""

import os 
import pandas as pd 
import seaborn as sns 
import matplotlib as mpl
import matplotlib.pyplot as plt 
import altair as alt 
import matplotlib.colors as colors
import numpy as np 

def from_kwargs(key, default, kwargs, rm = False):
    """
    This function will try to extract key from the kwargs, 
    and if it fails it will return default
    """
    try: 
        if rm == False: 
            r = kwargs[key]
        else: 
            r = kwargs.pop(key)
    except: 
        r = default
    return r 





ID_FEATURES = {
    "feature" : -2,
    "name" : -4, 
    "gene_id" : 0,
}
def read_tsv(filename):
    df = pd.read_table(filename)
    for f in ID_FEATURES:
        fdx = ID_FEATURES[f]
        df[f] = get_ID_feature(df, fdx )
    return df

def get_ID_feature(df, idx):
    tmp = list(df["target_id"])
    tmp = [i.split("|")[idx] for i in tmp]
    features = [i if i != "-" else None for i in tmp]
    return features 






def _prep_axis_df(df, axis_sample, **kwargs):
    """
    This function will prep a sub-dataframe for a given axis to be plotted in the scatterplot
    """
    sample_col = from_kwargs("sample_col", "sample", kwargs, rm = True)
    axis_df = df.query(f"{sample_col} == '{axis_sample}'")
    axis_df = axis_df.sort_values("target_id")
    return axis_df


def _prep_scatter_df(df, x_sample, y_sample, **kwargs):
    """
    This function prepares a total dataframe for x and y samples, and merges them so that they contain both the same entries in the same order
    """
    # perpare dataframes
    x_df = _prep_axis_df(df, x_sample, **kwargs)
    y_df = _prep_axis_df(df, y_sample, **kwargs)
    
    # merge dataframes together based on target_id
    new_df = x_df.merge(    
                        y_df, 
                        left_on = "target_id", 
                        right_on = "target_id", 
                        suffixes = (f"_{x_sample}", f"_{y_sample}")
                    )

    # now query for entries that have specified values in both x and y 
    # get x and y datasets to plot
    x_col = from_kwargs("x", f"tpm_{x_sample}", kwargs)
    y_col = from_kwargs("y", f"tpm_{y_sample}", kwargs)
    new_df = new_df.query(f"({x_col} == {x_col}) & ({y_col} == {y_col})")

    return new_df, x_col, y_col


def _prep_query_df(df, query):
    """
    This function prepares a subset dataframe for plotting according to specified queries
    """

    if not isinstance(query, (list, tuple, str)):
        raise TypeError("Queries have to be either a single string or a tuple of strings (len > 1) (which will be applied successively)")

    if isinstance(query, str):
        result = df.query(query)
    elif isinstance(query, (list, tuple)) and len(query) > 1:
        tmp_df = df.query(query[0])
        for q in query[1:]:
            tmp_df = tmp_df.query(q)
        result = tmp_df
    else: # in case the tuple only has one entry...
        result = df.query(query[0])
    
    return result 



def _get_query_params(query_name, query):
    """
    This function will extract the three valid parameters in a query: 
    Queries have to be passed on as dictionaries to avoid ambiguities
        color   |   color (str)
        q       |   the query itself (str)
        labels  |   whether or not to add labels (bool)
        legend  |   wether or not to add query to legend (bool)
    """
    # we can conveniently re-purpose from_kwargs for this ^^
    color = from_kwargs("color", None, query)
    q = from_kwargs("q", None, query)
    add_labels = from_kwargs("labels", False, query)
    legend = from_kwargs("legend", query_name, query)
    # if bools were given just use default values
    if legend == False: legend = None 
    elif legend == True: legend = query_name

    if q is None: 
        print(Warning(f"Query '{query_name}' does not have an actual query statement \"q\"!"))
    return color, q, add_labels, legend



def _label_queries(df, x_sample, x_col, y_col, **kwargs):
    """
    This function will add text labels to all datapoints that are part of a highlighted query-subset
    """
    # get the reference name column (has to be adjusted for renamed columns due to merged dfs from _prep_scatter_df)
    # as the order is the same for both dfs using x_sample or y_sample for referencing names is irrelevant
    name_col = from_kwargs("query_labels", "name", kwargs)
    name_col = f"{name_col}_{x_sample}"
    
    # get reference plot (in case of subplots)
    base_fig = from_kwargs("ax", plt, kwargs)

    # get the actual data points and labels to plot
    names = df[name_col]
    x_vals = df[x_col]
    y_vals = df[y_col]

    # get any offset for labels that may have been specified 
    x_offset = from_kwargs("query_label_xoff", 0.1, kwargs)
    y_offset = from_kwargs("query_label_yoff", 0.1, kwargs)
    label_size = from_kwargs("query_label_size", "small", kwargs)
    label_color = from_kwargs("query_label_color", "black", kwargs)

    # add labels
    for x, y, name in zip(x_vals, y_vals, names):
        base_fig.text(
            x + x_offset,
            y + y_offset,
            name,
            size = label_size,
            color = label_color
        )





def _scatter(df, x_sample, y_sample, line = True, add_labels = False, **kwargs):
    """
    This function perpares a scatterplot of two sample conditions (x_sample and y_sample) which are subsets of df
    Through kwargs can be adjusted: 
        x, y      the actual data columns used for plotting (default "tpm" )
        style     the sns.style used for plotting

    """
    # get reference plot (in case of subplots)
    base_fig = from_kwargs("ax", plt, kwargs)

    # perpare dataframes
    df, x_col, y_col = _prep_scatter_df(df, x_sample, y_sample, **kwargs)

    # get x and y datasets to plot
    x_vals = df[x_col]
    y_vals = df[y_col]
    
    # setup plot
    style = from_kwargs("style", "whitegrid", kwargs, rm = True)
    sns.set_theme(style=style)

    sns.scatterplot(
        x = x_vals, y = y_vals, 
        alpha = 0.5, 
        color = from_kwargs("color", None, kwargs),
        label = from_kwargs("label", None, kwargs),
        ax = from_kwargs("ax", None, kwargs),
    )
    if line == True: 
        min_overall = min([min(x_vals), min(y_vals)])
        max_overall = max([max(x_vals), max(y_vals)])
        line_vals = np.arange(min_overall, max_overall)
        base_fig.plot(
            
            line_vals, #(min(x_vals), max(x_vals)),
            line_vals, #(min(y_vals), max(y_vals)), 
            color = from_kwargs("line_color", "black", kwargs), 
            alpha = from_kwargs("line_alpha", None, kwargs), 
        )
    
    if add_labels == True: 
        _label_queries(df, x_sample, x_col, y_col, **kwargs)


def scatter(df, x_sample, y_sample, queries = None, **kwargs):
    """
    This function generates a scatterplot of the tpm counts of two samples (x_sample, y_sample)
    Using queries it is possible to additionaly plot colored subsets of the dataframe

    Using Queries for subsets: 
    Queries can be submitted as nested dictionaries in the format:

    {
     "query title" : { 
                       "q" : "the pandas query",   # (required) (string or tuple of strings, 
                                                     for successive queries)
                       "color" : "some_color",     # the color to be used (default None)
                       "labels" : True,            # label datapoints (default False)
                       "legend" : True,            # include subset in legend (default True)
                    }          
    }

    Successive Queries "q": 
    Queries can be multi-step. Simply provide a tuple of queries to "q" and they will be performed successively.
    """

    # get some kwargs out if they're present (so they don't cause trouble when we pass them on...)
    legend_loc = from_kwargs("legend_loc", None, kwargs, rm = True)
    title = from_kwargs("title", None, kwargs, rm = True)
    figsize = from_kwargs("figsize", None, kwargs, rm = True)
    traceback = from_kwargs("traceback", None, kwargs, rm = True)
    draw_guide_line = from_kwargs("line", True, kwargs, rm = True)
    
    # get base_fig in case of subplots
    base_fig = from_kwargs("ax", None, kwargs)
    if base_fig is None:
        fig, ax = plt.subplots(nrows = 1, figsize=figsize)
    else: 
        ax = base_fig
        
    # plot once the base set
    _scatter(df, x_sample, y_sample, color = "gray", line = draw_guide_line, add_labels = False, **kwargs)

    # plot any subset-queries
    if queries is not None: 
        for query in queries:
            try: 
                # prep sample queries
                tmp_query = queries[query]
                color, q, add_query_labels, legend_name = _get_query_params(query, tmp_query)
                tmp_df = _prep_query_df(df, q)
                _scatter(tmp_df, x_sample, y_sample, 
                        color = color, line = False, 
                        label = legend_name,
                        add_labels = add_query_labels,
                        **kwargs
                    )    

            except Exception as e: 
                if traceback: 
                    raise e
                else:
                    print(Warning(f"Query '{query}' could not be processed...\nUse traceback=True to view the full Exception"))
            
            hide_legend = from_kwargs("hide_legend", False, kwargs)
            if hide_legend == False:
                ax.legend(loc = legend_loc)
            else: 
                ax.get_legend().remove()

    # update final plot specs such as labels...
    ax.set( xlabel = from_kwargs("xlabel", x_sample, kwargs),
            ylabel = from_kwargs("ylabel", y_sample, kwargs),
            xlim = from_kwargs("xlim", (None, None), kwargs),
            ylim = from_kwargs("ylim", (None, None), kwargs),
            title = title
        )
    
